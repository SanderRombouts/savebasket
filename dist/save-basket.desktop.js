(function () {
    'use strict';

    angular
        .module('SaveBasket', [

            'ngAnimate',
            'xml',
            'Kinetix.Widgets.CustomContent',

            'SaveBasket.Templates',

            'SaveBasket.User',
            'SaveBasket.Offers',
            'SaveBasket.Modal',
            'SaveBasket.Trigger',
            'SaveBasket.Constants',
            'SaveBasket.Logic',
            'SaveBasket.Resources',
            'SaveBasket.Session.Persistence',
            'SaveBasket.SalesChannel',
            'SaveBasket.DataLayer'



        ])
        .config(['customContentProvider', 'SB_PATH', function(customContentProvider, SB_PATH) {
            customContentProvider.setOptions({
                endpointUrls: {
                    label: ['/', SB_PATH.channel,'/','mcsmambo.p?objecttype=rpc&m5NextUrl=mdortext&compatible=true'].join(''),
                    text: ['/', SB_PATH.channel,'/','mcsmambo.p?objecttype=rpc&m5NextUrl=mdortext&compatible=true'].join('')
                }
            });
        }])
        .run(['$rootScope', 'sbUser', 'SaveBasket', 'SB_EVENT', function($rootScope, sbUser, SaveBasket, SB_EVENT) {
            var userValidatedListener = $rootScope.$on(SB_EVENT.userValidated, function (event, user, doRetrieve) {
                doRetrieve = doRetrieve === undefined ? true : doRetrieve;
                if (doRetrieve) {
                    SaveBasket.retrieveAfterUserValidated();
                    userValidatedListener();
                }
            });
        }]);
})();
(function () {

    'use strict';


    angular.module('SaveBasket.Constants', [])


        /**
         * @ngdoc service
         * @name SaveBasket.Constants.SB_EVENTS
         * @description
         * Events used throughout the saveBasket module
         */
        .constant('SB_EVENT', {
            currentCommand      : 'SB_CURRENT_COMMAND',
            saveOffer           : 'SB_SAVE_OFFER',
            deleteOffer         : 'SB_DELETE_OFFER',
            modalClose          : 'SB_MODAL_CLOSE',
            userValidated       : 'SB_USER_VALIDATED',
            userValidationFailed: 'SB_USER_NOT_VALIDATED',
            validateUser        : 'SB_VALIDATE_USER',
            showBasket          : 'SB_SHOW_BASKET',
            changeOffer         : 'SB_CHANGE_OFFER',
            offersRetrieved     : 'SB_OFFERS_RETRIEVED',
            openBasket          : 'SB_OPEN_BASKET',
            closeModal          : 'SB_CLOSE_MODAL',
            dataLayerEvent      : 'SB_DATALAYER_EVENT'
        })
        .constant('SB_PERSISTENCE', {
            offers        : 'SB_OFFERS',
            selectedOffer : 'SB_SELECTED_OFFER',
            user          : 'SB_USER',
            salesAgent    : 'SB_SALES_AGENT',
            firstSave     : 'SB_FIRST_SAVE',
            busyRetrieving: 'SB_BUSY_RETRIEVING'
        })
        .constant('SB_PORTALS_LOGIN', {
            url: (function() {
                var url = location.hostname.toLowerCase();
                if (url.indexOf('rapid.shop.t-mobile') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('respip.shop.t-mobile') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('staging.shop.t-mobile') !== -1) {
                    return 'https://www-1a3.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('waterfall.shop.t-mobile') !== -1) {
                    return 'https://www-1a1.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('tmobile-') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else {
                    return 'https://www.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
            })()
        })
        //.constant('SB_DATALAYER', {
        //    package : {
        //        PackageActivationCost: undefined,
        //        PackageMID: undefined,
        //        PackageQuantity: '1',
        //        PackageContractDuration: undefined,
        //        PackageContractType: undefined,
        //        PackageMRC: undefined,
        //        PackageMRCDiscount: undefined,
        //        PackageOneTimeCost: undefined,
        //        PackageValue: undefined,
        //        Products: []
        //    },
        //    product : {
        //        ProductActivationCost: undefined,
        //        ProductBrand: undefined,
        //        ProductColor: undefined,
        //        ProductDescription: undefined,
        //        ProductListPrice: undefined,
        //        ProductMID: undefined,
        //        ProductMRC: undefined,
        //        ProductMRCDuration: undefined,
        //        ProductMRCDiscount: undefined,
        //        ProductMRCDiscountDuration: undefined,
        //        ProductMRCDiscountDescription: undefined,
        //        ProductName: undefined,
        //        ProductOneTimeCost: undefined,
        //        ProductType: undefined,
        //        ProductCode: undefined
        //    }
        //});
})();

(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer', []);
})();

(function () {

    'use strict';

    angular.module('SaveBasket.Logic', []);

})();
(function () {
    'use strict';

    angular
        .module('SaveBasket.Modal', [

            'ui.bootstrap'

        ]);
})();
(function () {

    'use strict';

    angular.module('SaveBasket.Offers', [
        'SaveBasket.Offers.Parser'
    ]);

})();
(function () {

    'use strict';

    angular.module('SaveBasket.Resources', [
        'ngResource'
    ]);

})();
(function () {

    'use strict';

    angular.module('SaveBasket.SalesChannel', [
        'SaveBasket.Resources'
    ]);

})();
(function () {
    'use strict';

    angular
        .module('SaveBasket.Trigger', []);
})();
(function () {

    'use strict';

    angular.module('SaveBasket.User', []);

})();
angular.module('SaveBasket.Templates', ['modals/templates/emptyBasketModal.tpl.html', 'modals/templates/infoOverlay.tpl.html', 'modals/templates/offerModal.tpl.html', 'modals/templates/loginOverlay.tpl.html', 'modals/templates/noOfferOverlay.tpl.html', 'modals/templates/replaceOverlay.tpl.html', 'offers/templates/sbOfferDisplay.tpl.html']);

angular.module('modals/templates/emptyBasketModal.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/emptyBasketModal.tpl.html',
    '<div class="sb-modal"><div class="sb-modal__container"><div class="desktop-wrapper-narrow"><div class="sb-modal__header"><div class="sb-modal__close" ng-click="close()"></div><div class="sb-modal__title"><span class="sb-hart"></span> <span custom-text="MCSGOURL.167">Je favorieten</span></div></div><hr><div class="sb-modal__content"><div class="sb-img-empty-basket center box-margin-vertical box-margin-center"></div><p class="center"><span custom-text="MCSGOURL.169">Hmmmm...<br>heb je nog niets in je favorieten?</span></p><div class="center box-margin-top"><button sb-trigger="save" class="btn btn-magenta"><span custom-text="MCSGOURL.168">Huidige keuze opslaan</span></button></div></div></div></div></div>');
}]);

angular.module('modals/templates/infoOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/infoOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent sb-info-modal"><div class="sb-modal__container"><div class="desktop-wrapper"><div sb-offer-count sb-trigger class="trigger-desktop"><span class="sb-hart"></span> <span class="sb-trigger-static-text">Favorieten</span></div><div class="sb-modal__content"><div class="content__header"><div class="sb-img-arrow-heart"></div><p class="sb-text-button-right text-styles text"><span custom-text="MCSGOURL.171">Hier vind je jouw favorieten</span></p><div class="sb-text-button-right text-styles"><button ng-click="close()" class="btn btn-magenta"><span custom-text="MCSGOURL.170">Ok&eacute;, ik snap het!</span></button></div></div></div></div></div></div>');
}]);

angular.module('modals/templates/offerModal.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/offerModal.tpl.html',
    '<div class="sb-modal sb-offer-modal"><div class="sb-modal__container"><div class="desktop-wrapper-narrow"><div class="sb-modal__header"><div class="sb-modal__close" ng-click="close()"></div><div class="sb-modal__title"><span class="sb-hart"></span> <span custom-text="MCSGOURL.167">Je favorieten</span></div></div><hr><div class="sb-modal__content"><sb-offer-display ng-repeat="offer in offers track by $index" offer="offer"></sb-offer-display></div></div></div></div>');
}]);

angular.module('modals/templates/loginOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/loginOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content"><p class="center bold">Om iets op te slaan in je eigen verlanglijstje moet je ingelogd zijn.</p><div class="center"><button ng-click="login()" class="btn btn-magenta">Login</button></div><div class="center"><button ng-click="close()" class="btn btn-inverse-blue box-margin-top">Ga verder</button></div></div></div></div>');
}]);

angular.module('modals/templates/noOfferOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/noOfferOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content center-content"><p class="center"><span custom-text="MCSGOURL.177">Om iets op te slaan op je favorieten moet je eerst een toestel selecteren</span></p><div class="center box-margin-top"><button ng-click="close()" class="btn btn-magenta"><span custom-text="MCSGOURL.170">Ok&eacute;, ik snap het!</span></button></div></div></div></div>');
}]);

angular.module('modals/templates/replaceOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/replaceOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content center-content"><div class="box-margin-vertical sb-img-offer-replace box-margin-center"></div><p class="center offer-replace-text"><span custom-text="MCSGOURL.173">Je hebt al een artikel in<br>je favorieten</span></p><p class="center"><span custom-text="MCSGOURL.174">Wil je die vervangen?</span></p><div class="center box-margin-top offer-replace-btn"><button ng-click="close()" class="btn btn-grey">Nee</button> <button sb-trigger="replace" class="btn btn-green">Ja</button></div></div></div></div>');
}]);

angular.module('offers/templates/sbOfferDisplay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('offers/templates/sbOfferDisplay.tpl.html',
    '<div class="sb-offer box-padding-vertical"><div class="sb-offer__image"><img ng-src="{{ offer.image }}" alt=""></div><div class="sb-offer__info"><div class="sb-offer__title"><h2>{{ offer.title }}</h2></div><div class="box-margin-top sb-offer__info-rateplan"><span class="magenta small">Abonnement&nbsp;{{ offer.rateplan.duration | number:0 }}&nbsp;mnd</span></div><div ng-if="offer.data.abodat" class="sb-offer__half box-margin-top"><p><span class="bold large" ng-bind="offer.data.abodat"></span> <span class="bold small">Internet</span> <span ng-if="offer.rateplan.showTShirtSize && offer.data.tshirtSize && offer.data.isUnlimited === \'no\'">(<span ng-bind-html="offer.data.tshirtSize" ng-class="{\'infinity-symbol\': isInfiniteSymbol(offer.data.tshirtSize)}"></span>)</span></p><p><span class="tiny grey" ng-bind="offer.data.abodatdown"></span></p></div><div ng-if="offer.minutes.abobmin" class="sb-offer__half box-margin-top"><p><span class="bold large" ng-bind="offer.minutes.abobmin"></span> <span class="bold large suffix" ng-bind="offer.minutes.suffix"></span></p><p><span class="tiny grey" ng-bind="offer.minutes.abobun"></span></p></div><div class="sb-offer__list box-margin-top"><ul><li ng-if="offer.insurance.description"><span class="tiny grey" ng-bind="offer.insurance.description"></span></li><li ng-repeat="addon in offer.addons"><span class="tiny grey" ng-bind="addon.name"></span></li></ul></div><div class="sb-offer__price box-margin-top"><p ng-if="offer.totalPrice"><span class="green">&euro;&nbsp;{{offer.totalPrice}}</span> <span class="green">/mnd</span></p><p class="small" ng-if="offer.handsetTotal"><span class="bold">Bijbetaling&nbsp;&euro;&nbsp;</span> <span class="bold" ng-bind="offer.handsetTotal"></span></p></div></div><div class="sb-offer__commands box-margin-top"><button class="sb-offer__commands--order btn btn-magenta" ng-click="orderOffer(offer)"><span custom-text="MCSGOURL.166">Bestellen</span></button> <button class="sb-offer__commands--edit btn btn-inverse-blue" ng-click="changeOffer(offer)"><span custom-text="MCSGOURL.165">Wijzig</span></button></div></div><hr>');
}]);

(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name SaveBasket
     * @description Main factory for interacting with saveBasket features
     */
    angular
        .module('SaveBasket')
        .factory('SaveBasket', SaveBasketService);

    function SaveBasketService(sbOffers, $rootScope, SB_EVENT, sbUserLogin, $timeout, sbUser, sbSessionPersistence, SB_PERSISTENCE, SB_ENV, sbDataLayer) {

        var firstSave         = true,
            currentCommand    = '',
            saveAfterRetrieve = false;


        /**
         * Run this function to fake a login on CA
         */
        var fakeLogin = function () {
            console.log('Fake Login! Comment!');
            $timeout(function () {
                sbUser.set({
                    validation: {
                        extra: {
                            'customer.phone': '+31624534888',
                            'customer.code': '1.13435668',
                            'customer.email': 'san@san.com'
                        }
                    }
                });
            });

        };
        //fakeLogin();

        function initDesktop() {
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.firstSave)) {
                firstSave = sbSessionPersistence.getItem(SB_PERSISTENCE.firstSave);
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        /**
         *
         */
        var saveOffer = function () {
            sbOffers.saveOffer();
        };

        /**
         * delete the offer passed
         */
        var deleteOffer = function (offer) {
            sbOffers.deleteOffer(offer);

            if (!sbOffers.offers.length) {
                $rootScope.$broadcast(SB_EVENT.modalClose);
            }
        };

        /**
         * Plots the offer passed into the function on the PDP
         * @param {object} offer - Object containing offer details.
         */
        var changeOffer = function (offer) {
            sbOffers.setSelectedOffer(offer);

            // don't close the modal on desktop... wait for the page to reload
            if (!SB_ENV.isDesktop) {
                $rootScope.$broadcast(SB_EVENT.modalClose);
            }
            sbOffers.navigateToOffer(offer);
        };
        var orderOffer  = function (offer) {
            sbOffers.setSelectedOffer(offer);

            // don't close the modal on desktop... wait for the page to reload
            if (!SB_ENV.isDesktop) {
                var listener = $rootScope.$on(SB_EVENT.closeModal, function () {
                    $rootScope.$broadcast(SB_EVENT.modalClose);
                    listener();
                });
            }
            sbOffers.orderOffer(offer);
        };

        /**
         * Retrieve offers for the current customer
         */
        var retrieveOffers = function () {
            sbOffers.retrieveOffers();
        };

        var retrieveAfterUserValidated = function () {
            // if we are redirected from portals login and need to save an offer, set saveAfterRetrieve to true.
            if (window.location.href.indexOf('savebasket=true') !== -1) {
                saveAfterRetrieve = true;
            }
            // after user validation retrieve the offers (if any)
            retrieveOffers();
        };

        /**
         * Event handlers
         */
        // @ToDo: Check the following listener. It seems to be redundant.
        // $rootScope.$on(SB_EVENT.saveOffer, function () {
        //     saveOffer();
        // });

        $rootScope.$on(SB_EVENT.currentCommand, function (event, command) {
            currentCommand = command;
        });

        $rootScope.$on(SB_EVENT.offersRetrieved, function () {
            if (sbOffers.offers.length) {
                firstSave = false;
            }
            if (saveAfterRetrieve) {

                $timeout(function () {
                    saveAfterRetrieve = false;
                    $rootScope.$broadcast(SB_EVENT.openBasket, 'save');
                }, 400);

            }
        });

        return {
            offerAvailable: sbOffers.offerAvailable,
            hasSelectedOffer: sbOffers.hasSelectedOffer,
            retrieveAfterUserValidated: retrieveAfterUserValidated,
            saveOffer: saveOffer,
            deleteOffer: deleteOffer,
            orderOffer: orderOffer,
            changeOffer: changeOffer,
            offerCount: function () {
                return sbOffers.getOfferCount();
            },
            login: sbUserLogin.login,
            maxOffers: sbOffers.getMaxOfferCount(),
            offers: sbOffers.offers,
            get firstSave() {
                return firstSave;
            },
            set firstSave(v) {
                firstSave = v;
                sbSessionPersistence.setItem(SB_PERSISTENCE.firstSave, firstSave);
            },
            currentCommand: currentCommand

        };
    }
    SaveBasketService.$inject = ['sbOffers', '$rootScope', 'SB_EVENT', 'sbUserLogin', '$timeout', 'sbUser', 'sbSessionPersistence', 'SB_PERSISTENCE', 'SB_ENV', 'sbDataLayer'];

})();

(function () {

    'use strict';

    angular.module('SaveBasket.Constants')

        /**
         * @ngdoc service
         * @name SaveBasket.Constants.SB_PATH
         * @description
         * Path Constants used throughout the saveBasket module
         */
        .constant('SB_PATH', {
            url: (function () {
                var path = window.location.pathname;
                return path;
            })(),
            channel: (function () {
                if (window.location.origin.indexOf('verlengen') !== -1) {
                    return 'ecr';
                }
                var path = window.location.pathname;
                return path.split('/')[1] || 'eca';
            })(),
            loginUrl: (function () {
                if (location.hostname.indexOf('tmobile') === -1) {
                    return location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/ws/';
                } else {
                    return location.protocol + '//' + location.hostname + ':8181/';

                }
            })(),
            widget: '',
            templates: ''
        })
        .constant('SB_ENV', {
            platform: 'desktop',
            isDesktop: true,
            isMobile: false
        })
        .constant('SB_MAMBO_USER', {
            retrieve: '83rt' + ( window.location.pathname.split('/')[1] || 'eca' ) + 'basket',
            save: '83svbasket',
            password: 'mtvmambo5'
        });

})();
(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer')
        .factory('sbDataLayerParser', SaveBasketDataLayerParser);

    function SaveBasketDataLayerParser(sbResources, $q) {

        function create(offer) {
            //TODO (created 28/04/16 14:59 by sanderrombouts) Priority: High - make sure offer.offerID exists #TODO

            return fillPackage(offer);
        }

        function addOfferID(packageObj, offer) {
            //TODO (created 28/04/16 14:55 by sanderrombouts) Priority: High - make sure offer.offerID exists #TODO
            packageObj[0].OfferID = offer.offerID;
            return packageObj;
        }

        function fillPackage(offer) {

            var defer = $q.defer(),
                options = {
                    art_id: offer.raw.globals[0].artId,
                    abo_id: offer.raw.globals[0].aboId,
                    xart_id: offer.raw.globals[0].combishow
                };

            sbResources.getDataLayer(options).then(
                function success(response) {
                    var data = mapNullToUndefined(response.data.mdodlpix.Shop.Packages);

                    data = addOfferID(data, offer);

                    defer.resolve(data);
                },
                function error(reason) {
                    defer.reject(reason)
                }
            );

            return defer.promise;
        }

        function mapNullToUndefined(data) {

            var obj;

            if (data instanceof Array) {
                obj = []
            } else if (typeof data === 'object') {
                obj = {}
            }

            _.each(data, function(val, key) {

                if (data[key] !== null && typeof data[key] !== 'string') {
                    obj[key] = mapNullToUndefined(val);
                } else {
                    obj[key] = val === null ? undefined : val;
                }
            });

            return obj;

        }

        return {
            create: create
        };
    }
    SaveBasketDataLayerParser.$inject = ['sbResources', '$q'];
})();
(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer')
        .factory('sbDataLayer', SaveBasketDataLayerService);

    function SaveBasketDataLayerService($rootScope, sbDataLayerParser, SB_EVENT, $window) {

        var addOffer = function (offer) {
            sbDataLayerParser.create(offer).then(
                function success(data) {
                    offer.dataLayer = data;
                    pushToDataLayer(data, 'add', offer);
                },
                function error(reason) {
                    console.error('Unable to load dataLayer: ', reason);
                }
            );
        };

        var deleteOffer = function (offer) {
            if (offer && offer.dataLayer) {
                pushToDataLayer(offer.dataLayer, 'remove');
                return;
            }

            sbDataLayerParser.create(offer).then(
                function success(data) {
                    pushToDataLayer(data, 'remove', offer);
                },
                function error(reason) {
                    console.error('Unable to load dataLayer: ', reason);
                }
            );
        };

        var pushToDataLayer = function (obj, action, offer) {
            var layer;
            if (action === 'add') {
                layer = {
                    event: 'addToCart',
                    Shop: {
                        ProductsInWishlist: obj
                    }
                };
            } else if (action === 'remove') {
                layer = {
                    event: 'removeFromCart',
                    Shop: {
                        ProductsRemoveFromWishlist: obj
                    }
                };
            }
            $window.dataLayer = $window.dataLayer || [];
            $window.dataLayer.push(layer);

            if (offer) {
                offer.dataLayer = layer.Shop.ProductsInWishlist;
            }

            $rootScope.$broadcast('dataLayerEvent', {action: action, layer: layer, offer: offer});
        };

        $rootScope.$on(SB_EVENT.saveOffer, function (event, offer) {
            addOffer(offer);
        });

        $rootScope.$on(SB_EVENT.deleteOffer, function (event, offer) {
            deleteOffer(offer);
        });

        return {};

    }
    SaveBasketDataLayerService.$inject = ['$rootScope', 'sbDataLayerParser', 'SB_EVENT', '$window'];
})();

(function () {

    'use strict';

    angular.module('SaveBasket.Logic')
        .factory('sbLogic', SaveBasketLogicService);

    function SaveBasketLogicService(SB_ENV) {

        var showBasket = false;

        // on desktop the
        if (SB_ENV.isDesktop) {
            showBasket = true;
        }

        //$rootScope.$on('$viewContentLoaded', function(event) {
//
        //    try {
        //        showBasket = event.currentScope.$state.current.data.showSaveBasket;
        //    } catch(e) {
        //        /**
        //         * Should be false, but in order to always show the triggers!!
        //         * @type {boolean}
        //         */
        //        showBasket = false;
        //    }
//
        //    $rootScope.$broadcast(SB_EVENT.showBasket, showBasket);
        //});

        return {
            showBasket : showBasket
        };
    }
    SaveBasketLogicService.$inject = ['SB_ENV'];

//
})();
(function() {

    'use strict';

    angular.module('SaveBasket')
        .controller('sbDefaultModalController', SaveBasketDefaultModalController)
        .controller('sbOffersModalController', SaveBasketOffersModalController);


    //SaveBasketDefaultModalController.$inject = ['$rootScope', '$scope', '$modalInstance', 'SB_EVENT'];
    function SaveBasketDefaultModalController($rootScope, $scope, $modalInstance, SB_EVENT, sbUser) {

        var close = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.user = sbUser.get();

        $scope.close = close;

        $rootScope.$on('$stateChangeStart', function () {
            $modalInstance.dismiss('cancel');
        });
        $rootScope.$on(SB_EVENT.modalClose, function () {
            $modalInstance.dismiss('cancel');
        });
    }
    SaveBasketDefaultModalController.$inject = ['$rootScope', '$scope', '$modalInstance', 'SB_EVENT', 'sbUser'];

    //SaveBasketOffersModalController.$inject = ['$rootScope', '$scope', '$modalInstance','SaveBasket', 'SB_EVENT'];
    function SaveBasketOffersModalController($rootScope, $scope, $modalInstance, SaveBasket, SB_EVENT) {

        var close = function () {
            $modalInstance.dismiss('cancel');
        };

        var login = function() {
            $rootScope.$broadcast(SB_EVENT.validateUser);
        };


        $scope.close = close;
        $scope.login = login;
        $scope.offerAvailable = SaveBasket.offerAvailable();

        $rootScope.$on('$stateChangeSuccess', function () {
            $modalInstance.dismiss('cancel');
        });
        $rootScope.$on(SB_EVENT.modalClose, function () {
            $modalInstance.dismiss('cancel');
        });

        $scope.offers = SaveBasket.offers;


    }
    SaveBasketOffersModalController.$inject = ['$rootScope', '$scope', '$modalInstance', 'SaveBasket', 'SB_EVENT'];

})();
(function () {
    'use strict';

    angular
        .module('SaveBasket.Modal')
        .factory('sbModal', SaveBasketModalService);

    function SaveBasketModalService(SaveBasket, $templateCache, $modal, $rootScope, SB_EVENT, sbUser) {

        var modals = {
            info: {
                template: $templateCache.get('modals/templates/infoOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            offers: {
                template: $templateCache.get('modals/templates/offerModal.tpl.html'),
                controller: 'sbOffersModalController'
            },
            emptyBasket: {
                template: $templateCache.get('modals/templates/emptyBasketModal.tpl.html'),
                controller: 'sbOffersModalController'
            },
            replaceOffer: {
                template: $templateCache.get('modals/templates/replaceOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            login: {
                template: $templateCache.get('modals/templates/loginOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            noOffer: {
                template: $templateCache.get('modals/templates/noOfferOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            }
        };
        var closeAllModals = function() {
            $rootScope.$broadcast(SB_EVENT.modalClose);
        };

        var openBasket = function(command) {

            closeAllModals();

            if (!sbUser.get().validation) {
                SaveBasket.login();
            }
            else if (!SaveBasket.offerAvailable() && (command === 'replace' || command === 'save')) {
                $modal.open(modals.noOffer);
            }
            else if (command === 'replace') {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            }
            else if (command === 'save' && SaveBasket.hasSelectedOffer()) {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            }
            else if (command === 'save' && SaveBasket.offerCount() >= SaveBasket.maxOffers) {
                $modal.open(modals.replaceOffer);
            }
            else if (command === 'save' && SaveBasket.firstSave) {
                SaveBasket.firstSave = false;
                SaveBasket.saveOffer();
                $modal.open(modals.info);
            } else if (command === 'save') {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            } else if (SaveBasket.offerCount()) {
                $modal.open(modals.offers);
            } else {
                $modal.open(modals.emptyBasket);
            }
        };

        $rootScope.$on(SB_EVENT.openBasket, function(event, command) {
            openBasket(command);
        });

        return {
            openBasket: openBasket
        };
    }
    SaveBasketModalService.$inject = ['SaveBasket', '$templateCache', '$modal', '$rootScope', 'SB_EVENT', 'sbUser'];
})
();

/**
 * Created by sanderrombouts on 11/01/16.
 */
(function () {

    'use strict';

    angular.module('SaveBasket.Offers')
        .factory('sbFormInfo', SaveBasketFormInfo);

    //SaveBasketOffersService.$inject = ['sbOfferParser', 'sbResources'];

    function SaveBasketFormInfo() {


        function getFormValues() {

            var form = {};
            $('#form input').each(function() {
                var $this = $(this);
                form[$this.attr('name')] = $this.val();
            });

            return form;
        }

        function getKey(key) {
            return getFormValues()[key];
        }
        function hasKey(key) {
            return !!getFormValues()[key];
        }


        return {
            get: getKey,
            hasKey: hasKey,
            getAll: getFormValues
        };

    }


})();
(function () {

    'use strict';

    angular.module('SaveBasket.Offers.Parser', [])
        .factory('sbOfferParser', SaveBasketOfferParser);

    function SaveBasketOfferParser($rootScope, x2js, SB_PATH, sbUser, sbResources, sbFormInfo, $q, sbSalesChannel, SB_EVENT) {

        var createPdpParams = function () {
            return {};
        };
        var getRatePlanAddon    = function (array, type) {
            return _.filter(array, function (addon) {
                return addon.arId.toLowerCase() === type.toLowerCase();
            });
        };
        var getFieldFromAddon   = function (addon, field) {
            return addon && addon[0] && addon[0][field] ? addon[0][field] : '';
        };
        var decimals            = function (val, num) {
            return val.toFixed(num);
        };
        var currency            = function (val) {
            return decimals(val, 2).replace('.', ',');
        };

        var parseMdorJSON = function (data, retrieveResponse) {
            var o = {},
                addon;

            if (retrieveResponse) {
                o.offerID = retrieveResponse.offerID;
                o.retrieveRsponse = retrieveResponse;
            }

            o.origin       = 'app';

            try {
                addon          = data.handset[0];
                o.id           = addon.artId;
                o.handsetTotal = parseFloat(addon.priceOnceUnformatted) > 0 ? addon.priceOnceUnformatted : '';
                o.image = addon.artImage;

                if (addon.artIsSim) {
                    o.title = data.rateplan[0].artDesc + ' Only';
                } else {
                    o.title = addon.artSupplier + ' ' + addon.artDesc;
                }
            } catch(e) {
                console.debug('No handset available');
            }
            var rateplan = data.rateplan[0];

            o.rateplan = {
                duration: rateplan.aboTermInMonths,
                showTShirtSize: rateplan.showTShirtSize
            };

            var arr = data.rateplan[0].package;

            addon  = getRatePlanAddon(arr, 'DAT');
            o.data = {
                abodat: getFieldFromAddon(addon, 'artDesc'),
                abodatdown: getFieldFromAddon(addon, 'artDatDown'),
                tshirtSize: getFieldFromAddon(addon, 'tshirtSize'),
                isUnlimited: getFieldFromAddon(addon, 'isUnlimited')
            };

            addon     = getRatePlanAddon(arr, 'MIN');
            o.minutes = {
                abobun: getFieldFromAddon(addon, 'artDesc'),
                abobmin: getFieldFromAddon(addon, 'artAbobmin'),
                suffix: getFieldFromAddon(addon, 'artAbobmin') && getFieldFromAddon(addon, 'artAbobmin').toLowerCase().indexOf('onbeperkt') === -1 ? 'Minuten' : ''
            };

            addon       = data.insurance;
            o.insurance = {
                description: getFieldFromAddon(addon, 'artDesc')
            };

            addon       = data.mbverdeler;
            o.mbverdeler = {
                description: getFieldFromAddon(addon, 'artDesc')
            };

            o.addons     = _.chain(data.addons)
                .filter(function (addOn) {
                    return addOn.arId !== 'AB';
                })
                .map(function (addon) {
                    var o  = {};
                    o.name = addon.artDesc;
                    return o;
                })
                .value();
            var totals   = data.totals[0];
            o.totalPrice = currency(parseFloat(totals.totalMonthUnformatted) + parseFloat(totals.totalInsuranceUnformatted));

            o.raw = data;

            return o;
        };

        var createOfferObject = function (offerXML, addOfferFunc) {

            var defer = $q.defer();

            if (!offerXML) {

                sbResources.getFullProduct().then(
                    function (data) {

                        if (!data.mdorjson) {
                            defer.reject('no data');
                            return defer.promise;
                        }

                        defer.resolve(parseMdorJSON(data.mdorjson));
                    },
                    function (error) {

                        defer.reject(error);
                    });

            }
            else if (offerXML.basket.response.result.resultCode !== '0') {

                defer.reject('No result from TMNL');

            }
            else if (offerXML.basket) {

                var promises = [];

                _.forEach(offerXML.basket.offers, function (offer) {

                    var params = {};
                    var o      = {};

                    o.offerID = offer.offerID;
                    o.retrieveRsponse = offer;

                    var device = findOfferLineWithField(offer, 'ab_agexport', 'HANDSET') || findOfferLineWithField(offer, 'ab_agexport', 'SIM');

                    if (!device.art_id || device.available !== 'True') {
                        o.error = {
                            id: 1,
                            internalMessage: 'No tangible product found'
                        };
                        //addOfferFunc(o);
                        return;
                    }
                    params.art_id = device.art_id;

                    o.configObj = _.chain(offer.offerline)
                        .filter(function (offerline) {
                            return offerline.ab_agexport === 'SERVICE' || offerline.ab_agexport === 'RATEPLAN';
                        })
                        .map(function (offerline) {
                            return {
                                id: offerline.art_id,
                                available: offerline.available === 'True',
                                baseArticle: offerline.base_article,
                                offerline: offerline
                            };
                        }).value();

                    params.xart_id = _.map(_.filter(o.configObj, 'available'), 'id').join(',');
                    var init_abo_id = findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'base_article') || findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'art_id');
                    params.init_abo_id = init_abo_id;

                    promises.push(sbResources.getFullProduct(params).then(
                        function (data) {


                            if (!data.mdorjson) {
                                o.error = {
                                    id: 3,
                                    internalMessage: 'Something went wrong parsing the offer!',
                                    errorMessage: ''
                                };
                                addOfferFunc(o);
                            } else {
                                addOfferFunc(parseMdorJSON(data.mdorjson, offer));
                            }
                        },
                        function (e) {

                            o.error = {
                                id: 3,
                                internalMessage: 'Something went wrong parsing the offer!',
                                errorMessage: e
                            };
                            addOfferFunc(o);
                        }));
                });
                $q.all(promises).then(function() {
                    defer.resolve();
                });

            }
            else {
                defer.reject('Something went wrong parsing the offers');
            }
            return defer.promise;
        };

        /// function to pare after a retrieve //
        function findOfferLineWithField(offer, field, value, f) {

            var result = _.find(offer.offerline, function (offerLine) {
                return offerLine[field] === value;
            });
            if (result && f) {
                return result[f];
            } else {
                return result;
            }

        }

        var replaceRatePlanWithBase = function (xart, rateplan, base) {

            if (base && rateplan) {

                xart = xart.replace(rateplan, base);

            }
            return xart;

        };

        var navigateToOffer = function (offer) {

            var globals  = offer.raw.globals[0],
                rateplan = offer.raw.rateplan[0];

            var form      = sbFormInfo.getAll();
            var form_xart = _.chain(form.xart_id.split(','))
                .filter(function (product) {
                    return product.toLowerCase().indexOf('hcopy') === -1;
                })
                .sortBy()
                .value()
                .join(',');

            var offer_xart = _.chain(globals.combishow.split(','))
                .sortBy()
                .value()
                .join(',');

            var combishow = replaceRatePlanWithBase(offer_xart, rateplan.artId, rateplan.artBaseId);

            // if the offer is the same as the current PDP, just close the modal
            if (form.art_id !== globals.artId || form_xart !== offer_xart) {
                // modify the combishow to prevent lock-in. eg. replace 24brpp002 with 24brpp001 and
                // let the bestPriceCheck change it to 24brpp002 again

                window.location = encodeURI('/' + SB_PATH.channel + '/mcsmambo.p?m5nexturl=RAPRD&art_id=' + globals.artId + '&xart_id=' + combishow);
            } else {
                $rootScope.$broadcast(SB_EVENT.modalClose);
            }
        };

        var orderOffer = function (offer) {

            var query = {
                M5NextUrl: 'MDORAOPT',
                Currurl: 'RAPRD',
                art_id: offer.id,
                bag_add: offer.id,
                xart_id: offer.raw.globals[0].combishow,
                abo_id: offer.raw.globals[0].aboId,
                init_abo_id: offer.raw.rateplan[0].artBaseId
            };

            query = _.map(query, function (val, key) {
                return key + '=' + val;
            }).join('&');

            window.location = encodeURI('/' + SB_PATH.channel + '/mcsmambo.p?' + query);

        };

        var createOfferXML = function (offers) {
            var XML        = {
                    basket: {
                        customercode: sbUser.getInfo('code'),
                        MSISDN: sbUser.getInfo('phone'),
                        emailAddress: sbUser.getInfo('email'),
                        salesAgentCode: sbSalesChannel.getSalesAgent().ab_dealer,
                        salesChannel: SB_PATH.channel.indexOf('r') === 1 ? 'TSHOP' : 'ESLS'
                    }
                },
                offerArray = [];

            // create offerArray
            _.forEach(offers, function (offer) {
                var raw    = offer.raw;
                var result = [].concat(raw.rateplan, raw.handset, raw.addons, raw.hip, raw.rateplan[0].package, raw.insurance, raw.mbverdeler);

                var termy = raw.rateplan[0].aboTermInMonths;

                result = _.chain(result)
                    .compact()
                    .filter(function (item) {
                        return !!item.artId;
                    })
                    .map(function (item) {
                        var obj      = {};
                        obj.art_bbid = item.artBBID;

                        if (item.isDevice) {
                            obj.art_custom1 = '';
                            obj.ab_agexport = item.artIsSim ? 'SIM' : 'HANDSET';
                        } else if (item.isRatePlan) {
                            obj.art_custom1 = 'A';
                            obj.ab_agexport = 'RATEPLAN';
                        } else {
                            obj.art_custom1 = 'O';
                            obj.ab_agexport = 'SERVICE';
                        }
                        obj.abotermy = termy;

                        return obj;
                    })
                    .value();

                var ol = {offerline: result};
                offerArray.push(ol);
            });

            XML.basket.offers = {offer: offerArray};

            return x2js.json2xml_str(XML);
        };

        var offerAvailable = function () {

            return !!sbFormInfo.get('art_id');
        };

        function getSelectedProductObject() {

            var all = sbFormInfo.getAll(),
                xart_id = all.xart_id;

            if (all.init_abo_id && all.abo_id) {
                xart_id = xart_id.replace(all.abo_id, all.init_abo_id);
            }


            if (offerAvailable()) {
                var obj = {
                    id: sbFormInfo.get('art_id'),
                    description: sbFormInfo.get('art_desc'),
                    config: xart_id
                };

                return obj;
            }

            return false;

        }

        var createRetrieveXML = function () {
            var XML = {
                basket: {
                    customercode: sbUser.getInfo('code'),
                    MSISDN: sbUser.getInfo('phone'),
                    cust_fc: sbUser.getInfo('value'),
                    cust_fc2: sbUser.getInfo('value2'),
                    cust_email: sbUser.getInfo('email')
                }
            };
            return x2js.json2xml_str(XML);
        };

        return {
            offerAvailable: offerAvailable,
            createOfferObject: createOfferObject,
            pdpParams: createPdpParams,
            navigateToOffer: navigateToOffer,
            createOfferXML: createOfferXML,
            createRetrieveXML: createRetrieveXML,
            getSelectedProductObject: getSelectedProductObject,
            orderOffer: orderOffer
        };

    }
    SaveBasketOfferParser.$inject = ['$rootScope', 'x2js', 'SB_PATH', 'sbUser', 'sbResources', 'sbFormInfo', '$q', 'sbSalesChannel', 'SB_EVENT'];
})();
(function () {

    'use strict';

    angular.module('SaveBasket.Offers')
        .factory('sbOffers', SaveBasketOffersService);

    //SaveBasketOffersService.$inject = ['sbOfferParser', 'sbResources'];

    function SaveBasketOffersService(sbOfferParser, sbResources, x2js, $rootScope, SB_EVENT, SB_ENV, SB_PERSISTENCE, sbSessionPersistence) {

        var offers         = [],
            maxOffers      = 1,
            selectedOffer,
            busyRetrieving = false;

        var navigateToOffer = function (offer) {
            sbOfferParser.navigateToOffer(offer);
        };

        var orderOffer = function (offer) {
            sbOfferParser.orderOffer(offer);
        };

        var compareOffer  = function (offer1, offer2) {
            return JSON.stringify(offer1.selected) === JSON.stringify(offer2.selected);
        };
        var compareOffers = function (offer) {
            var i = -1;
            _.some(offers, function (o, index) {
                if (compareOffer(o, offer)) {
                    i = index;
                    return true;
                }
            });
            return i;
        };

        var setSelectedOffer   = function (offer, persist) {
            persist = persist === undefined ? true : persist;

            $rootScope.$broadcast(SB_EVENT.changeOffer, offer);
            selectedOffer = offer;

            if (persist) {
                sbSessionPersistence.setItem(SB_PERSISTENCE.selectedOffer, selectedOffer);
            }
        };
        var getSelectedOffer   = function () {
            return selectedOffer;
        };
        var resetSelectedOffer = function () {
            $rootScope.$broadcast(SB_EVENT.changeOffer, null);
            selectedOffer = null;
            sbSessionPersistence.removeItem(SB_PERSISTENCE.selectedOffer);
        };

        var getOfferCount = function () {
            return offers.length;
        };

        /**
         * Delete the current offer in the wish list and broadcast deleteOffer event
         * @param {object} offer - Object containing current offer details.
         */
        var deleteOffer = function (offer) {
            $rootScope.$broadcast(SB_EVENT.deleteOffer, offer);

            if (selectedOffer && compareOffer(offer, selectedOffer)) {
                resetSelectedOffer();
            }
            offers.splice(offers.indexOf(offer), 1);

            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);
        };

        var addOffer = function (offer) {

            offers[0] = offer;
            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);

            $rootScope.$broadcast(SB_EVENT.addOffer, offer);
        };

        /**
         * Saves the offer in the wish list and broadcast events
         * @param {object} offer - Object containing offer details.
         */
        var _saveOffer = function (offer) {
            // If there is already an offer, it will be replaced with a new one;
            // therefore, broadcast the deleteOffer event with the previous offer as arg
            if(offers[0]) {
                $rootScope.$broadcast(SB_EVENT.deleteOffer, offers[0]);
            }

            // Assign the new offer and broadcast the saveOffer event with the new offer as arg
            offers[0] = offer;
            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);

            sbResources.save(sbOfferParser.createOfferXML(offers)).then(
                function success(data) {
                    try {
                        offer.offerID = data.response.results.result.offerID;
                    } catch (e) {
                        offer.offerID = undefined;
                        console.warn('no offerID available :', e);
                    }
                    offer.saveResponse = data;
                    offers[0] = offer;
                    sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);
                    $rootScope.$broadcast(SB_EVENT.saveOffer, offer);
                    resetSelectedOffer();
                },
                function error(reason) {
                    console.error('Problems saving:', reason);
                }
            );

        };

        var saveOffer = function () {

            if (!sbOfferParser.offerAvailable()) {
                return;
            }

            sbOfferParser.createOfferObject().then(
                function (offer) {

                    if (busyRetrieving) {
                        var listener = $rootScope.$on(SB_EVENT.offersRetrieved, function () {
                            _saveOffer(offer);
                            listener();
                        });
                    } else {
                        _saveOffer(offer);
                    }
                }
            );
        };

        var retrieveOffers = function () {

            busyRetrieving = true;
            sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, true);

            sbResources.retrieve(sbOfferParser.createRetrieveXML())
                .then(function (data) {
                    var xml = x2js.xml_str2json(data);
                    if (xml) {
                        sbOfferParser.createOfferObject(xml, addOffer).then(
                            function success() {
                                $rootScope.$broadcast(SB_EVENT.offersRetrieved);
                                busyRetrieving = false;
                                sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, false);
                            }, function error(e) {
                                $rootScope.$broadcast(SB_EVENT.offersRetrieved);
                                console.debug(e);
                                busyRetrieving = false;
                                sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, false);
                            });
                    }
                });
        };

        function initDesktop() {
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.busyRetrieving)) {
                busyRetrieving = sbSessionPersistence.getItem(SB_PERSISTENCE.busyRetrieving);
            }


            if (busyRetrieving) {
                retrieveOffers();
            }

            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.offers)) {
                offers = sbSessionPersistence.getItem(SB_PERSISTENCE.offers);
            }
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.selectedOffer)) {
                setSelectedOffer(sbSessionPersistence.getItem(SB_PERSISTENCE.selectedOffer), false);
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        return {
            offerAvailable: sbOfferParser.offerAvailable,
            getOfferCount: getOfferCount,
            getMaxOfferCount: function () {
                return maxOffers;
            },
            saveOffer: saveOffer,
            retrieveOffers: retrieveOffers,
            compareOffers: compareOffers,
            deleteOffer: deleteOffer,
            offers: offers,
            setSelectedOffer: setSelectedOffer,
            getSelectedOffer: getSelectedOffer,
            hasSelectedOffer: function () {
                return !!getSelectedOffer();
            },
            resetSelectedOffer: resetSelectedOffer,
            getPdpParams: sbOfferParser.pdpParams,
            navigateToOffer: navigateToOffer,
            orderOffer: orderOffer
        };
    }
    SaveBasketOffersService.$inject = ['sbOfferParser', 'sbResources', 'x2js', '$rootScope', 'SB_EVENT', 'SB_ENV', 'SB_PERSISTENCE', 'sbSessionPersistence'];

})();

(function () {

    'use strict';

    angular.module('SaveBasket.Offers')
        .directive('sbOfferDisplay', SaveBasketOfferDisplayDirective);

    function SaveBasketOfferDisplayDirective(SB_PATH) {
        return {

            restrict: 'E',
            scope: {
                offer: '=',
                path: '='
            },
            controller: SaveBasketOfferDisplayController,
            templateUrl: SB_PATH.templates + 'offers/templates/sbOfferDisplay.tpl.html'

        };
    }
    SaveBasketOfferDisplayDirective.$inject = ['SB_PATH'];

    function SaveBasketOfferDisplayController($scope, SaveBasket) {
        $scope.deleteOffer = function(offer) {
            SaveBasket.deleteOffer(offer);
        };
        $scope.orderOffer  = function(offer) {
            SaveBasket.orderOffer(offer);
        };
        $scope.changeOffer = function(offer) {
            SaveBasket.changeOffer(offer);
        };
        $scope.isInfiniteSymbol = function (symbol) {
            var infinity = ['&#8734;', '&infin;', '∞', '&#x221E;'];
            
            return infinity.indexOf(symbol) > -1;
        };
    }
    SaveBasketOfferDisplayController.$inject = ['$scope', 'SaveBasket'];



})();
(function () {

    'use strict';

    angular.module('SaveBasket.Session.Persistence', ['ngCookies']).factory('sbSessionPersistence', SaveBasketSessionPersistenceService);
    /**
     * @ngdoc service
     * @name sbSessionPersistence
     *
     * @description
     * Temporarily persists objects in a value-key map
     */
    function SaveBasketSessionPersistenceService(SB_ENV) {
        var cookieName = 'sbSessionStorage',
            hashMap    = [];

        function init() {
            hashMap = JSON.parse(sessionStorage.getItem(cookieName));
            if (!angular.isArray(hashMap)) {
                hashMap = [];
            }
        }

        function clearSession() {
            sessionStorage.setItem(cookieName, '');
        }

        function update() {
            sessionStorage.setItem(cookieName, JSON.stringify (hashMap));
        }

        if (SB_ENV.isDesktop) {
            init();
            window.onbeforeunload = function () {
                //sessionStorage.setItem(cookieName, JSON.stringify (hashMap));
            };
        }

        function getItem(key) {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                result,
                i;
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k === key) {
                    result = item.v;
                }
            }
            return result;
        }

        function setItem(key, data) {
            if (SB_ENV.isMobile) {
                return;
            }
            removeItem(key);
            hashMap.push({k: key, v: data});
            update();
        }

        function removeItem(key) {
            if (SB_ENV.isMobile) {
                return;
            }
            var item,
                i,
                _hashMap = [];
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k !== key) {
                    _hashMap.push(item);
                }
            }
            hashMap = _hashMap;
            update();
        }

        function getAllItems() {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                i,
                result = [];
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                result.push(item.v);
            }
            return result;
        }

        function getAllItemsWithKeys() {
            if (SB_ENV.isMobile) {
                return false;
            }
            return hashMap;
        }

        function clearItems() {
            if (SB_ENV.isMobile) {
                return false;
            }
            hashMap = [];
            update();
        }

        function itemExists(key) {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                i,
                result = false;
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k === key) {
                    result = true;
                    break;
                }
            }
            return result;
        }

        return {
            setItem: setItem,
            getItem: getItem,
            removeItem: removeItem,
            getAllItems: getAllItems,
            getAllItemsWithKeys: getAllItemsWithKeys,
            clearItems: clearItems,
            itemExists: itemExists,
            clear: clearSession
        };
    }
    SaveBasketSessionPersistenceService.$inject = ['SB_ENV'];
})();
(function () {

    'use strict';

    /**
     * @module SaveBasket.Resources
     * @description Services for retrieving and saving offers and
     * getting a product json for the product in a offer
     */



    angular.module('SaveBasket.Resources')
        .factory('sbResources', SaveBasketResourcesService);

    function SaveBasketResourcesService($http, SB_PATH, $q, sbFormInfo, SB_MAMBO_USER, x2js) {

        /**
         * Saves an offer
         *
         * @param offerXML
         * @returns {*|r.promise|promise|m.promise|{then, catch, finally}}
         */
        var save = function (offerXML) {

            var defer = $q.defer();

            $http({
                method: 'post',
                data: offerXML,
                headers: {
                    'Content-Type': 'text/xml'
                },
                url: '/' + SB_PATH.channel + '/mcsgoxml.p?methodname=xtnsvbsk&pmu_id=' + SB_MAMBO_USER.save + '&pmu_pwd=' + SB_MAMBO_USER.password
            }).then(
                function succes(response) {
                    defer.resolve(x2js.xml_str2json(response.data));
                },
                function error(reason) {
                    defer.reject(reason);
                }
            );

            return defer.promise;

        };

        var retrieve = function (xml) {

            var defer = $q.defer();

            $http({
                method: 'post',
                data: xml,
                headers: {
                    'Content-Type': 'text/xml'
                },
                url: '/' + SB_PATH.channel + '/mcsgoxml.p?methodname=ttnxabsk&pmu_id=' + SB_MAMBO_USER.retrieve + '&pmu_pwd=' + SB_MAMBO_USER.password
            }).then(
                function succes(response) {
                    defer.resolve(response.data);
                },
                function error(reason) {
                    defer.reject(reason);
                }
            );

            return defer.promise;
        };

        /** Retrieve the full json object for a given product **/
        var getFullProduct = function (params) {

            var defer = $q.defer();
           $http({
               method: 'get',
               url: '/' + SB_PATH.channel + '/mcsmambo.p',
               params:  angular.extend({
                   'M5NextUrl': 'MDORJSON',
                   'M5CurrUrl' : 'RAPRD',
                   'art_id' : sbFormInfo.get('art_id'),
                   'xart_id' : sbFormInfo.get('xart_id'),
                   'objectType' : 'RPC',
                   'M5WebChoice' : 'CMT_91',
                   'init_abo_id' : sbFormInfo.get('init_abo_id')
               }, params)
           }).then(
               function succes(response) {
                   defer.resolve(response.data);
               },
               function error(reason) {
                   defer.reject(reason);
               }
           );

           return defer.promise;

        };

        function getUser() {
            var defer = $q.defer();

            $http({
                    method: 'get',
                    url: '/' + SB_PATH.channel + '/mcsmambo.p',
                    params: {
                        'objecttype': 'rpc',
                        'm5NextUrl': 'mdorcust',
                        'compatible': true
                    }
                }).then(
                function success(response) {
                    defer.resolve(response.data);
                },
                function error(reason) {
                    defer.reject(reason);
                });

            return defer.promise;
        }

        function getSalesAgent() {
            var defer = $q.defer();

            $http({
                method: 'get',
                url: '/' + SB_PATH.channel + '/mcsmambo.p',
                params: {
                    'objecttype': 'rpc',
                    'm5NextUrl': 'mdoagent',
                    'compatible': true
                }
            }).then(
                function success(response) {
                    var data = response.data;
                    if (data.mdoagent && data.mdoagent.tt_dealercode && data.mdoagent.tt_dealercode[0]) {
                        defer.resolve(data.mdoagent.tt_dealercode[0]);
                    } else {
                        defer.reject('no SalesAgent object present');
                    }

                },
                function error(reason) {
                    defer.reject(reason);
                });

            return defer.promise;
        }
        function getDataLayer(options) {
            var defer = $q.defer(),
                params = {
                    'objectType': 'rpc',
                    'm5NextUrl': 'mdodlpix',
                    'compatible': true,
                    'Pixelreload': true
                };

            params = angular.extend(params, options);

            $http({
                method: 'get',
                url: '/' + SB_PATH.channel + '/mcsmambo.p',
                params: params,
                cache: false
            }).then(
                function success(response) {

                    var data = response;

                    defer.resolve(data);

                },
                function error(reason) {
                    defer.reject(reason);
                });

            return defer.promise;
        }

        return {
            save: save,
            retrieve: retrieve,
            getFullProduct: getFullProduct,
            getUser: getUser,
            getSalesAgent: getSalesAgent,
            getDataLayer: getDataLayer
        };
    }
    SaveBasketResourcesService.$inject = ['$http', 'SB_PATH', '$q', 'sbFormInfo', 'SB_MAMBO_USER', 'x2js'];

})();
/**
 * Intercepts the response from the textblock json and converts it to usable format
 */
(function(){
    'use strict';

    angular.module('SaveBasket.Resources')
        .factory('customContentHttpInterceptor', customContentHttpInterceptor)
        .config(['$httpProvider', function($httpProvider) {
            $httpProvider.interceptors.push(customContentHttpInterceptor);
        }]);

    function customContentHttpInterceptor() {

        var interceptor = {

            response: function(response) {
                if (response.config.url.indexOf('m5NextUrl=mdortext') !== -1) {
                    response.data = formatData(response.data);
                }
                return response;
            }

        };
        return interceptor;

        function formatData(data) {

            var obj = {};

            try {
                _.each(data.mdortext.tt_text, function(item) {
                    if (item.Text) {
                        obj[item.ID] = item.Text;
                    }

                });
            } catch(e) {

            }

            return obj;
        }

    }


})();

(function () {

    'use strict';

    angular.module('SaveBasket.SalesChannel')
        .factory('sbSalesChannel', SaveBasketSalesChannelService);


    function SaveBasketSalesChannelService(sbResources, sbSessionPersistence, SB_ENV, SB_PERSISTENCE) {

        var salesAgent;

        function initDesktop() {

            if(sbSessionPersistence.itemExists( SB_PERSISTENCE.salesAgent )){
                setSalesAgent(sbSessionPersistence.getItem( SB_PERSISTENCE.salesAgent ), false);
            } else {
                sbResources.getSalesAgent().then(
                    function succes(salesAgent) {
                        setSalesAgent(salesAgent, true);
                    },
                    function error(reason) {
                        console.log('Failed to retrieve the SalesAgent', reason);
                    });
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        function setSalesAgent(agent, persist) {

            salesAgent = agent;

            persist = typeof persist === 'undefined' ? true : persist;

            if (persist) {
                sbSessionPersistence.setItem( SB_PERSISTENCE.salesAgent, salesAgent );
            }
        }
        function getSalesAgent() {
            return salesAgent || {};
        }



        return {

            getSalesAgent: getSalesAgent

        };
    }
    SaveBasketSalesChannelService.$inject = ['sbResources', 'sbSessionPersistence', 'SB_ENV', 'SB_PERSISTENCE'];

})();
(function () {
    'use strict';

    angular.module('SaveBasket.Trigger')
        .directive('sbOfferCount', saveBasketOfferCountDirective);

    function saveBasketOfferCountDirective(SaveBasket) {
        return {
            restrict: 'A',
            link: function(scope, element) {

                setAttribute(SaveBasket.offerCount());

                scope.$watch(function() {
                    return SaveBasket.offerCount();
                }, function(val) {
                    setAttribute(val);
                });

                function setAttribute(nval, oval) {
                    if (nval !== oval) {
                        if (nval) {
                            element.attr('sb-offer-count-value', nval);
                        } else {
                            element.removeAttr('sb-offer-count-value');
                        }

                    }
                }

            }
        };
    }
    saveBasketOfferCountDirective.$inject = ['SaveBasket'];

})();
(function () {

    'use strict';

    angular.module('SaveBasket.Trigger')
        .directive('sbTrigger', saveBasketTriggerDirective);

    function saveBasketTriggerDirective(SaveBasket, sbModal, $rootScope, SB_EVENT) {
        return {
            restrict: 'EA',
            scope: {
                command: '@sbTrigger',
                openBasket: '=openBasket',
                addText: '@',
                changeText: '@'
            },
            controller: ['$rootScope', function($rootScope) {
                //ToDo - find a better solution! - Used for configurable text on trigger link.
                $rootScope.addOffer = !SaveBasket.hasSelectedOffer();
                $rootScope.changeOffer = SaveBasket.hasSelectedOffer();
            }],
            link: function (scope, element) {

                element.removeClass('hidden');

                if (SaveBasket.hasSelectedOffer()) {
                    setText(scope.changeText);
                } else {
                    setText(scope.addText);
                }

                $rootScope.$on(SB_EVENT.changeOffer, function(event, offer) {

                    if (offer) {
                        setText(scope.changeText);
                    } else {
                        setText(scope.addText);
                    }
                });

                $rootScope.$on(SB_EVENT.showBasket, function(event, show) {
                    if (show) {
                        element.removeClass('hidden');
                    } else {
                        element.addClass('hidden');
                    }
                });

                element.on('click', function () {

                    $rootScope.$broadcast(SB_EVENT.currentCommand, scope.command);

                    if (typeof scope.openBasket === 'undefined' || scope.openBasket) {
                        sbModal.openBasket(scope.command);
                    }

                });

                function setText(text) {
                    if (text) {
                        element.find('.sb-trigger-text').text(text);
                    }

                }
            }
        };
    }
    saveBasketTriggerDirective.$inject = ['SaveBasket', 'sbModal', '$rootScope', 'SB_EVENT'];

})();
(function () {

    'use strict';


    angular.module('SaveBasket.User')
        .factory('sbUserLogin', SaveBasketUserLoginService);

    /**
     * @ngdocs service
     * @name sbUserLogin
     * @description Handles the login for a ECA customer. The login for a ECR customer is handled the normal way by the application
     * If a cryptoticket is available on ECA, it means that the customer is redirected back from the Portals verification.
     */
    function SaveBasketUserLoginService(SB_PATH, SB_EVENT, $rootScope, sbOfferParser, SB_PORTALS_LOGIN) {

        var currentCommand = '';
        $rootScope.$on(SB_EVENT.currentCommand, function(event, command) {
            currentCommand = command;
        });

        function login() {
            window.location = SB_PORTALS_LOGIN.url + generateRedirectUrl();
        }

        function generateRedirectUrl() {
            var url = window.location.origin + window.location.pathname,
                obj = sbOfferParser.getSelectedProductObject();

            if (!obj) {
                return encodeURIComponent(url);

            } else {
                url = window.location.origin + '/' + SB_PATH.channel + '/RAPRD/'+ obj.description + '/' + obj.id + '.html?xart_id=' + obj.config;

                if (currentCommand === 'save') {
                    url += '&savebasket=true';
                }

                return encodeURIComponent(url);
            }

        }
        $rootScope.$on(SB_EVENT.validateUser, function() {
            login();
        });


        return {
            login: login
        };

    }
    SaveBasketUserLoginService.$inject = ['SB_PATH', 'SB_EVENT', '$rootScope', 'sbOfferParser', 'SB_PORTALS_LOGIN'];

})();
(function () {

    'use strict';

    angular.module('SaveBasket.User')
        .service('sbUser', SaveBasketUserService);

    function SaveBasketUserService($rootScope, SB_EVENT, SB_PERSISTENCE, sbSessionPersistence, SB_ENV, sbResources, $timeout) {
        var user          = {},
            userValidated = false;

        function initDesktop() {
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.user)) {
                user = sbSessionPersistence.getItem(SB_PERSISTENCE.user);
                set(user, false);
            } else {
                sbResources.getUser().then(
                    function succes(user) {
                        user = user.mdorcust.tt_customer[0];

                        var userObj = {
                            validation: {
                                extra: {
                                    'customer.code': user.customerCode,
                                    'customer.phone': user.cust_msisdn,
                                    'customer.email': user.cust_email,
                                    'customer.value': user.cust_fc,
                                    'customer.value2': user.cust_fc2
                                }
                            }
                        };
                        set(userObj);
                    },
                    function error(reason) {
                        console.log('Failed to retrieve the user', reason);
                    });
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        function get() {
            return user;
        }

        function set(u, persist) {

            persist = persist === undefined ? true : persist;

            // validate if the user is really known, mambo sends a user object even if the user is not know at all
            if (!getInfo('code', u) && !getInfo('email', u) && !getInfo('msisdn', u)) {
                return;
            }

            user = u;

            if (!userValidated) {
                userValidated = true;
                $timeout(function () {
                    $rootScope.$broadcast(SB_EVENT.userValidated, u, persist);
                });

            }
            if (persist) {
                sbSessionPersistence.setItem(SB_PERSISTENCE.user, u);
            }
        }

        function getInfo(field, userObj) {

            var info = '';
            userObj  = userObj || user;

            try {
                info = userObj.validation.extra['customer.' + field] || '';
            } catch (e) {

            }

            return info;

        }

        return {
            get: get,
            set: set,
            getInfo: getInfo
        };
    }
    SaveBasketUserService.$inject = ['$rootScope', 'SB_EVENT', 'SB_PERSISTENCE', 'sbSessionPersistence', 'SB_ENV', 'sbResources', '$timeout'];

})();