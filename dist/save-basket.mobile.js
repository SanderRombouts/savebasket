
(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name SaveBasket
     * @description The main module for SaveBasket
     */

    angular
        .module('SaveBasket', [

            'ngAnimate',
            'xml',

            /* Mobile Only */
            'Tmobile.Services.SelectedProduct',
            'Tmobile.Services.Description',
            'Tmobile.Services.Channel',

            'SaveBasket.Templates',

            'SaveBasket.User',
            'SaveBasket.Offers',
            'SaveBasket.Modal',
            'SaveBasket.Trigger',
            'SaveBasket.Constants',
            'SaveBasket.Logic',
            'SaveBasket.Resources',
            'SaveBasket.Session.Persistence',
            'SaveBasket.DataLayer'


        ])
        .run(function($rootScope, TM_AUTH_EVENTS, sbUser, SaveBasket, SB_EVENT) {
            $rootScope.$on(TM_AUTH_EVENTS.authenticated, function ($event, customer) {
                sbUser.set(customer);
            });
            var userValidatedListener = $rootScope.$on(SB_EVENT.userValidated, function (event, user, doRetrieve) {
                doRetrieve = doRetrieve === undefined ? true : doRetrieve;
                if (doRetrieve) {
                    SaveBasket.retrieveAfterUserValidated();
                    userValidatedListener();
                }
            });
        });
})();

(function () {

    'use strict';


    angular.module('SaveBasket.Constants', [])


        /**
         * @ngdoc service
         * @name SaveBasket.Constants.SB_EVENTS
         * @description
         * Events used throughout the saveBasket module
         */
        .constant('SB_EVENT', {
            currentCommand      : 'SB_CURRENT_COMMAND',
            saveOffer           : 'SB_SAVE_OFFER',
            deleteOffer         : 'SB_DELETE_OFFER',
            modalClose          : 'SB_MODAL_CLOSE',
            userValidated       : 'SB_USER_VALIDATED',
            userValidationFailed: 'SB_USER_NOT_VALIDATED',
            validateUser        : 'SB_VALIDATE_USER',
            showBasket          : 'SB_SHOW_BASKET',
            changeOffer         : 'SB_CHANGE_OFFER',
            offersRetrieved     : 'SB_OFFERS_RETRIEVED',
            openBasket          : 'SB_OPEN_BASKET',
            closeModal          : 'SB_CLOSE_MODAL',
            dataLayerEvent      : 'SB_DATALAYER_EVENT'
        })
        .constant('SB_PERSISTENCE', {
            offers        : 'SB_OFFERS',
            selectedOffer : 'SB_SELECTED_OFFER',
            user          : 'SB_USER',
            salesAgent    : 'SB_SALES_AGENT',
            firstSave     : 'SB_FIRST_SAVE',
            busyRetrieving: 'SB_BUSY_RETRIEVING'
        })
        .constant('SB_PORTALS_LOGIN', {
            url: (function() {
                var url = location.hostname.toLowerCase();
                if (url.indexOf('rapid.shop.t-mobile') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('respip.shop.t-mobile') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('staging.shop.t-mobile') !== -1) {
                    return 'https://www-1a3.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('waterfall.shop.t-mobile') !== -1) {
                    return 'https://www-1a1.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('tmobile-') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else {
                    return 'https://www.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
            })()
        })
        //.constant('SB_DATALAYER', {
        //    package : {
        //        PackageActivationCost: undefined,
        //        PackageMID: undefined,
        //        PackageQuantity: '1',
        //        PackageContractDuration: undefined,
        //        PackageContractType: undefined,
        //        PackageMRC: undefined,
        //        PackageMRCDiscount: undefined,
        //        PackageOneTimeCost: undefined,
        //        PackageValue: undefined,
        //        Products: []
        //    },
        //    product : {
        //        ProductActivationCost: undefined,
        //        ProductBrand: undefined,
        //        ProductColor: undefined,
        //        ProductDescription: undefined,
        //        ProductListPrice: undefined,
        //        ProductMID: undefined,
        //        ProductMRC: undefined,
        //        ProductMRCDuration: undefined,
        //        ProductMRCDiscount: undefined,
        //        ProductMRCDiscountDuration: undefined,
        //        ProductMRCDiscountDescription: undefined,
        //        ProductName: undefined,
        //        ProductOneTimeCost: undefined,
        //        ProductType: undefined,
        //        ProductCode: undefined
        //    }
        //});
})();

(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer', []);
})();

(function () {

    'use strict';

    angular.module('SaveBasket.Logic', []);

})();
(function () {
    'use strict';

    angular
        .module('SaveBasket.Modal', [

            'ui.bootstrap'

        ]);
})();
(function () {

    'use strict';

    angular.module('SaveBasket.Offers', [
        'SaveBasket.Offers.Parser'
    ]);

})();
(function () {

    'use strict';

    angular.module('SaveBasket.Resources', [
        'ngResource'
    ]);

})();
(function () {
    'use strict';

    angular
        .module('SaveBasket.Trigger', []);
})();
(function () {

    'use strict';

    angular.module('SaveBasket.User', []);

})();
angular.module('SaveBasket.Templates', ['modals/templates/loginOverlay.tpl.html', 'modals/templates/emptyBasketModal.tpl.html', 'modals/templates/infoOverlay.tpl.html', 'modals/templates/offerModal.tpl.html', 'modals/templates/noOfferOverlay.tpl.html', 'modals/templates/replaceOverlay.tpl.html', 'offers/templates/sbOfferDisplay.tpl.html']);

angular.module('modals/templates/loginOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/loginOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content"><p class="center bold">Om iets op te slaan in je eigen verlanglijstje moet je ingelogd zijn.</p><div class="center"><button ng-click="login()" class="btn btn-magenta">Login</button></div><div class="center"><button ng-click="close()" class="btn btn-inverse-blue box-margin-top">Ga verder</button></div></div></div></div>');
}]);

angular.module('modals/templates/emptyBasketModal.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/emptyBasketModal.tpl.html',
    '<div class="sb-modal"><div class="sb-modal__container"><div class="sb-modal__header"><div class="sb-modal__close" ng-click="close()"></div><div class="sb-modal__title"><span class="sb-hart"></span> <span custom-text="MCSGOURL.167">Je favorieten</span></div></div><hr><div class="sb-modal__content"><div class="sb-img-empty-basket center box-margin-vertical box-margin-center"></div><p class="center"><span custom-text="MCSGOURL.169">Hmmmm...<br>heb je nog niets in je favorieten?</span></p><div class="center box-margin-top"><button sb-trigger="save" class="btn btn-magenta"><span custom-text="MCSGOURL.168">Huidige keuze opslaan</span></button></div></div></div></div>');
}]);

angular.module('modals/templates/infoOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/infoOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent sb-info-modal"><div class="sb-modal__container"><div sb-offer-count sb-trigger class="sb-trigger"><span class="sb-hart"></span></div><div class="sb-modal__content"><div class="content__header"><div class="sb-img-arrow-heart"></div><p class="center"><span custom-text="MCSGOURL.171">Hier vind je jouw favorieten</span></p></div><div class="center"><button ng-click="close()" class="btn btn-magenta"><span custom-text="MCSGOURL.170">Ok&eacute;, ik snap het!</span></button></div><div class="content__footer"><p class="center"><span custom-text="MCSGOURL.172">....of rond direct af</span></p><div class="sb-img-arrow-order"></div></div></div></div></div>');
}]);

angular.module('modals/templates/offerModal.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/offerModal.tpl.html',
    '<div class="sb-modal"><div class="sb-modal__container"><div class="sb-modal__header"><div class="sb-modal__close" ng-click="close()"></div><div class="sb-modal__title"><span class="sb-hart"></span> <span custom-text="MCSGOURL.167">Je favorieten</span></div></div><hr><div class="sb-modal__content"><sb-offer-display ng-repeat="offer in offers track by $index" offer="offer"></sb-offer-display></div></div></div>');
}]);

angular.module('modals/templates/noOfferOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/noOfferOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content center-content"><p class="center"><span custom-text="MCSGOURL.177">Om iets op te slaan op je favorieten moet je eerst een toestel selecteren</span></p><div class="center box-margin-top"><button ng-click="close()" class="btn btn-magenta"><span custom-text="MCSGOURL.170">Ok&eacute;, ik snap het!</span></button></div></div></div></div>');
}]);

angular.module('modals/templates/replaceOverlay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('modals/templates/replaceOverlay.tpl.html',
    '<div class="sb-modal sb-modal--transparent"><div class="sb-modal__container"><div class="sb-modal__content center-content"><div class="box-margin-vertical sb-img-offer-replace box-margin-center"></div><p class="center offer-replace-text"><span custom-text="MCSGOURL.173">Je hebt al een artikel in<br>je favorieten</span></p><p class="center"><span custom-text="MCSGOURL.174">Wil je die vervangen?</span></p><div class="center box-margin-top offer-replace-btn"><button ng-click="close()" class="btn btn-grey">Nee</button> <button sb-trigger="replace" class="btn btn-green">Ja</button></div></div></div></div>');
}]);

angular.module('offers/templates/sbOfferDisplay.tpl.html', []).run(['$templateCache', function($templateCache) {
  'use strict';
  $templateCache.put('offers/templates/sbOfferDisplay.tpl.html',
    '<div class="sb-offer sb-offer-error box-padding-vertical" ng-if="offer.error" ng-switch="offer.error.id"><h2 ng-switch-when="1">{{ offer.error.internalMessage }}</h2></div><div class="sb-offer box-padding-vertical" ng-if="!offer.error"><div class="sb-offer__image"><img ng-src="{{ offer.image }}" alt="" ng-click="changeOffer(offer)"></div><div class="sb-offer__info" ng-click="changeOffer(offer)"><h2>{{ offer.title }}</h2><div class="box-margin-top sb-offer__info-rateplan"><span class="magenta small">Abonnement&nbsp;{{ offer.rateplan.duration | number:0 }}&nbsp;mnd</span></div><div ng-if="offer.data.abodat" class="sb-offer__half box-margin-top"><p><span class="bold large" ng-bind="offer.data.abodat"></span> <span class="bold small">Internet</span> <span ng-if="offer.rateplan.drlh_show_size && offer.data.drlh_size && !offer.data.art_isunlimited">(<span ng-bind-html="offer.data.drlh_size" ng-class="{\'infinity-symbol\': isInfiniteSymbol(offer.data.drlh_size)}"></span>)</span></p><p><span class="tiny grey" ng-bind="offer.data.abodatdown"></span></p></div><div ng-if="offer.minutes.abobun" class="sb-offer__half box-margin-top"><p><span class="bold large" ng-bind="offer.minutes.abobmin"></span> <span class="bold small" ng-if="offer.minutes.abobmin !== \'Onbeperkt\'">Minuten</span></p><p><span class="tiny grey" ng-bind="offer.minutes.abobun"></span></p></div><div class="sb-offer__list box-margin-top"><ul><li ng-if="offer.insurance[0].products[0]"><span class="tiny grey" ng-bind="offer.insurance[0].products[0].description"></span></li><li ng-if="offer.insurance.description"><span class="tiny grey" ng-bind="offer.insurance.description" ng-class="{strike: !offer.insurance.available}"></span></li><li ng-repeat="addon in offer.receipt.ratePlan.addOns" ng-if="offer.receipt.ratePlan.addOns"><span class="tiny grey" ng-bind="addon.name"></span></li><li ng-repeat="addon in offer.addOns" ng-if="offer.addOns"><span class="tiny grey" ng-bind="addon.name" ng-class="{strike: !addon.available}"></span></li></ul></div><div class="sb-offer__price box-margin-top"><p ng-if="offer.receipt.finalPricing.monthlyTotal"><span class="magenta" ng-bind="offer.receipt.finalPricing.monthlyTotal + offer.insurance[0].products[0].monthlyFee + offer.insurance.monthlyFee | euroCurrency"></span> <span class="small magenta" custom-label="RAPRD_MPRICE">/mnd</span></p><p class="small" ng-if="offer.receipt.finalPricing.oneoffHandsetTotal"><span>Bijbetaling&nbsp;</span> <span ng-bind="offer.receipt.finalPricing.oneoffHandsetTotal | euroCurrency"></span></p></div></div><div class="sb-offer__commands box-margin-top"><button class="sb-offer__commands--edit btn btn-inverse-blue" ng-click="changeOffer(offer)"><span custom-text="MCSGOURL.165">Wijzig</span></button> <button class="sb-offer__commands--order btn btn-magenta" ng-click="orderOffer(offer)"><span custom-text="MCSGOURL.166">Bestellen</span></button></div></div><hr>');
}]);

(function () {
    'use strict';

    /**
     * @ngdoc service
     * @name SaveBasket
     * @description Main factory for interacting with saveBasket features
     */
    angular
        .module('SaveBasket')
        .factory('SaveBasket', SaveBasketService);

    function SaveBasketService(sbOffers, $rootScope, SB_EVENT, sbUserLogin, $timeout, sbUser, sbSessionPersistence, SB_PERSISTENCE, SB_ENV, sbDataLayer) {

        var firstSave         = true,
            currentCommand    = '',
            saveAfterRetrieve = false;


        /**
         * Run this function to fake a login on CA
         */
        var fakeLogin = function () {
            console.log('Fake Login! Comment!');
            $timeout(function () {
                sbUser.set({
                    validation: {
                        extra: {
                            'customer.phone': '+31624534888',
                            'customer.code': '1.13435668',
                            'customer.email': 'san@san.com'
                        }
                    }
                });
            });

        };
        //fakeLogin();

        function initDesktop() {
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.firstSave)) {
                firstSave = sbSessionPersistence.getItem(SB_PERSISTENCE.firstSave);
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        /**
         *
         */
        var saveOffer = function () {
            sbOffers.saveOffer();
        };

        /**
         * delete the offer passed
         */
        var deleteOffer = function (offer) {
            sbOffers.deleteOffer(offer);

            if (!sbOffers.offers.length) {
                $rootScope.$broadcast(SB_EVENT.modalClose);
            }
        };

        /**
         * Plots the offer passed into the function on the PDP
         * @param {object} offer - Object containing offer details.
         */
        var changeOffer = function (offer) {
            sbOffers.setSelectedOffer(offer);

            // don't close the modal on desktop... wait for the page to reload
            if (!SB_ENV.isDesktop) {
                $rootScope.$broadcast(SB_EVENT.modalClose);
            }
            sbOffers.navigateToOffer(offer);
        };
        var orderOffer  = function (offer) {
            sbOffers.setSelectedOffer(offer);

            // don't close the modal on desktop... wait for the page to reload
            if (!SB_ENV.isDesktop) {
                var listener = $rootScope.$on(SB_EVENT.closeModal, function () {
                    $rootScope.$broadcast(SB_EVENT.modalClose);
                    listener();
                });
            }
            sbOffers.orderOffer(offer);
        };

        /**
         * Retrieve offers for the current customer
         */
        var retrieveOffers = function () {
            sbOffers.retrieveOffers();
        };

        var retrieveAfterUserValidated = function () {
            // if we are redirected from portals login and need to save an offer, set saveAfterRetrieve to true.
            if (window.location.href.indexOf('savebasket=true') !== -1) {
                saveAfterRetrieve = true;
            }
            // after user validation retrieve the offers (if any)
            retrieveOffers();
        };

        /**
         * Event handlers
         */
        // @ToDo: Check the following listener. It seems to be redundant.
        // $rootScope.$on(SB_EVENT.saveOffer, function () {
        //     saveOffer();
        // });

        $rootScope.$on(SB_EVENT.currentCommand, function (event, command) {
            currentCommand = command;
        });

        $rootScope.$on(SB_EVENT.offersRetrieved, function () {
            if (sbOffers.offers.length) {
                firstSave = false;
            }
            if (saveAfterRetrieve) {

                $timeout(function () {
                    saveAfterRetrieve = false;
                    $rootScope.$broadcast(SB_EVENT.openBasket, 'save');
                }, 400);

            }
        });

        return {
            offerAvailable: sbOffers.offerAvailable,
            hasSelectedOffer: sbOffers.hasSelectedOffer,
            retrieveAfterUserValidated: retrieveAfterUserValidated,
            saveOffer: saveOffer,
            deleteOffer: deleteOffer,
            orderOffer: orderOffer,
            changeOffer: changeOffer,
            offerCount: function () {
                return sbOffers.getOfferCount();
            },
            login: sbUserLogin.login,
            maxOffers: sbOffers.getMaxOfferCount(),
            offers: sbOffers.offers,
            get firstSave() {
                return firstSave;
            },
            set firstSave(v) {
                firstSave = v;
                sbSessionPersistence.setItem(SB_PERSISTENCE.firstSave, firstSave);
            },
            currentCommand: currentCommand

        };
    }

})();

(function () {

    'use strict';

    angular.module('SaveBasket.Constants')

        /**
         * @ngdoc service
         * @name SaveBasket.Constants.SB_PATH
         * @description
         * Path Constants used throughout the saveBasket module
         */
        .constant('SB_PATH', {
            url: (function(){
                if (location.hostname.indexOf('tmobile') === -1) {
                    return location.protocol + '//' + location.hostname + (location.port ? ':'+location.port : '') + '/ws/';
                } else {
                    //return location.protocol + '//' + location.hostname + ':8181/';
                    return 'https://mobile-rapid.shop.t-mobile.nl/ws/';

                }
            })(),
            loginUrl: (function(){
                if (location.hostname.indexOf('tmobile') === -1) {
                    return location.protocol + '//' + location.hostname + (location.port ? ':'+location.port : '') + '/ws/';
                } else {
                    //return location.protocol + '//' + location.hostname + ':8181/';
                    return 'https://mobile-rapid.shop.t-mobile.nl/ws/';

                }
            })(),
            widget: 'scripts/widgets/save-basket/',
            templates: ''
        })
        .constant('SB_ENV', {
            platform: 'mobile',
            isDesktop: false,
            isMobile: true
        });

})();
(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer')
        .factory('sbDataLayerParser', SaveBasketDataLayerParser);

    function SaveBasketDataLayerParser(googleTagDayaLayer, $q) {

        function create(offer) {

            return fillPackage(offer);
        }

        function addOfferID(packageObj, offer) {
            packageObj[0].OfferID = offer.offerID;
            return packageObj;
        }

        function fillPackage(offer) {

            var defer = $q.defer();

            var products = offer.selected.map(function (item) {
                return item.type !== 'color' && item.type !== 'AB-SWITCH' && item.products.length ? item.products[0] : false;
            }).filter(function (item) {
                return item.id && item.id !== 'zero-item';
            });

            products[0].finalPricing = offer.selectedProductObject.finalPricing;


            var packageObj = googleTagDayaLayer.createPackageObject(products);

            packageObj = addOfferID(packageObj, offer);

            defer.resolve(packageObj);

            return defer.promise;
        }

        return {
            create: create
        }
    }
})();

(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer')
        .factory('sbDataLayer', SaveBasketDataLayerService);

    function SaveBasketDataLayerService($rootScope, sbDataLayerParser, SB_EVENT, $window) {

        var addOffer = function (offer) {
            sbDataLayerParser.create(offer).then(
                function success(data) {
                    offer.dataLayer = data;
                    pushToDataLayer(data, 'add', offer);
                },
                function error(reason) {
                    console.error('Unable to load dataLayer: ', reason);
                }
            );
        };

        var deleteOffer = function (offer) {
            if (offer && offer.dataLayer) {
                pushToDataLayer(offer.dataLayer, 'remove');
                return;
            }

            sbDataLayerParser.create(offer).then(
                function success(data) {
                    pushToDataLayer(data, 'remove', offer);
                },
                function error(reason) {
                    console.error('Unable to load dataLayer: ', reason);
                }
            );
        };

        var pushToDataLayer = function (obj, action, offer) {
            var layer;
            if (action === 'add') {
                layer = {
                    event: 'addToCart',
                    Shop: {
                        ProductsInWishlist: obj
                    }
                };
            } else if (action === 'remove') {
                layer = {
                    event: 'removeFromCart',
                    Shop: {
                        ProductsRemoveFromWishlist: obj
                    }
                };
            }
            $window.dataLayer = $window.dataLayer || [];
            $window.dataLayer.push(layer);

            if (offer) {
                offer.dataLayer = layer.Shop.ProductsInWishlist;
            }

            $rootScope.$broadcast('dataLayerEvent', {action: action, layer: layer, offer: offer});
        };

        $rootScope.$on(SB_EVENT.saveOffer, function (event, offer) {
            addOffer(offer);
        });

        $rootScope.$on(SB_EVENT.deleteOffer, function (event, offer) {
            deleteOffer(offer);
        });

        return {};

    }
})();

(function () {

    'use strict';

    angular.module('SaveBasket.Logic')
        .factory('sbLogic', SaveBasketLogicService);

    //SaveBasketLogicService.$inject = ['$rootScope', 'SB_EVENT', 'tmProduct'];

    function SaveBasketLogicService($rootScope, SB_EVENT, tmProduct, Params, tmChannel) {

        var showBasket  = false;
        var channelInfo = {};

        $rootScope.$on(tmChannel.currentChannelChangeEvent, function (e, channel) {
            channelInfo = channel;
        });

        $rootScope.$on('$viewContentLoaded', function (event) {

            tmChannel.channelInfo().then(
                function success(channel) {

                    channelInfo = channel;

                    try {
                        showBasket = event.currentScope.$state.current.data.showSaveBasket && channel.extra.enable_savebasket;
                    } catch (e) {
                        /**
                         * Should be false, but on order to always show the triggers!!
                         * @type {boolean}
                         */
                        showBasket = false;
                    }

                    $rootScope.$broadcast(SB_EVENT.showBasket, showBasket);
                }
            );

        });

        function resetRatePlan() {
            tmProduct.ratePlan.resetRatePlan();
            tmProduct.hip.reset();
            Params.reset();
        }

        return {
            resetRatePlan: resetRatePlan,
            showBasket: showBasket,
            channelInfo: channelInfo
        };
    }

})();
(function() {

    'use strict';

    angular.module('SaveBasket')
        .controller('sbDefaultModalController', SaveBasketDefaultModalController)
        .controller('sbOffersModalController', SaveBasketOffersModalController);


    //SaveBasketDefaultModalController.$inject = ['$rootScope', '$scope', '$modalInstance', 'SB_EVENT'];
    function SaveBasketDefaultModalController($rootScope, $scope, $modalInstance, SB_EVENT, sbUser) {

        var close = function () {
            $modalInstance.dismiss('cancel');
        };

        $scope.user = sbUser.get();

        $scope.close = close;

        $rootScope.$on('$stateChangeStart', function () {
            $modalInstance.dismiss('cancel');
        });
        $rootScope.$on(SB_EVENT.modalClose, function () {
            $modalInstance.dismiss('cancel');
        });
    }

    //SaveBasketOffersModalController.$inject = ['$rootScope', '$scope', '$modalInstance','SaveBasket', 'SB_EVENT'];
    function SaveBasketOffersModalController($rootScope, $scope, $modalInstance, SaveBasket, SB_EVENT) {

        var close = function () {
            $modalInstance.dismiss('cancel');
        };

        var login = function() {
            $rootScope.$broadcast(SB_EVENT.validateUser);
        };


        $scope.close = close;
        $scope.login = login;
        $scope.offerAvailable = SaveBasket.offerAvailable();

        $rootScope.$on('$stateChangeSuccess', function () {
            $modalInstance.dismiss('cancel');
        });
        $rootScope.$on(SB_EVENT.modalClose, function () {
            $modalInstance.dismiss('cancel');
        });

        $scope.offers = SaveBasket.offers;


    }

})();
(function () {
    'use strict';

    angular
        .module('SaveBasket.Modal')
        .factory('sbModal', SaveBasketModalService);

    function SaveBasketModalService(SaveBasket, $templateCache, $modal, $rootScope, SB_EVENT, sbUser) {

        var modals = {
            info: {
                template: $templateCache.get('modals/templates/infoOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            offers: {
                template: $templateCache.get('modals/templates/offerModal.tpl.html'),
                controller: 'sbOffersModalController'
            },
            emptyBasket: {
                template: $templateCache.get('modals/templates/emptyBasketModal.tpl.html'),
                controller: 'sbOffersModalController'
            },
            replaceOffer: {
                template: $templateCache.get('modals/templates/replaceOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            login: {
                template: $templateCache.get('modals/templates/loginOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            },
            noOffer: {
                template: $templateCache.get('modals/templates/noOfferOverlay.tpl.html'),
                controller: 'sbOffersModalController'
            }
        };
        var closeAllModals = function() {
            $rootScope.$broadcast(SB_EVENT.modalClose);
        };

        var openBasket = function(command) {

            closeAllModals();

            if (!sbUser.get().validation) {
                SaveBasket.login();
            }
            else if (!SaveBasket.offerAvailable() && (command === 'replace' || command === 'save')) {
                $modal.open(modals.noOffer);
            }
            else if (command === 'replace') {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            }
            else if (command === 'save' && SaveBasket.hasSelectedOffer()) {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            }
            else if (command === 'save' && SaveBasket.offerCount() >= SaveBasket.maxOffers) {
                $modal.open(modals.replaceOffer);
            }
            else if (command === 'save' && SaveBasket.firstSave) {
                SaveBasket.firstSave = false;
                SaveBasket.saveOffer();
                $modal.open(modals.info);
            } else if (command === 'save') {
                SaveBasket.saveOffer();
                $modal.open(modals.offers);
            } else if (SaveBasket.offerCount()) {
                $modal.open(modals.offers);
            } else {
                $modal.open(modals.emptyBasket);
            }
        };

        $rootScope.$on(SB_EVENT.openBasket, function(event, command) {
            openBasket(command);
        });

        return {
            openBasket: openBasket
        };
    }
})
();

(function () {

    'use strict';

    angular.module('SaveBasket.Offers.Parser', [
            'Tmobile.Services.Product'
        ])
        .factory('sbOfferParser', SaveBasketOfferParser);

    function SaveBasketOfferParser(x2js, tmReceipt, tmSelectedProduct, tmLinks, tmDescription, $state, sbUser, sbLogic, sbResources, Params, tmPdp, tmRatePlanPricing, tmPricing, tmRatePlan, SB_PATH, $q, TM_RATE_PLAN, $rootScope, $timeout, SB_EVENT) {

        var debug = true;

        debug = function (what, description) {
            if (debug) {
                console.log(description + ' :', what);
            }
        };

        var createConfig    = function (products) {
            return _.map(_.filter(products, function (product) {
                return product.type !== 'HCL' && product.type !== 'color' && product.type !== 'UPR';
            }), function (product) {
                return product.products[0].id;
            }).join();
        };
        var createPdpParams = function (offer) {
            return {
                id: offer.id,
                config: offer.origin === 'app' ? createConfig(offer.selected) : offer.pdpConfig,
                description: tmDescription.getUrlDescription(offer.raw)
            };
        };

        var findAllSelected = function (links, arr) {
            arr = arr || [];
            _.each(links, function (l) {
                // cloud !!
                l.products = _.filter(l.products, function(p) {
                    return p.selected;
                });
                if (l.hasOwnProperty('selectedPlan') && l.selected) {
                        arr.push(l);
                } else if (l.products[0]) {
                    if ((!l.hasOwnProperty('selected') && l.products[0].selected) ||
                        (l.hasOwnProperty('selected') && l.selected === true)) {
                        arr.push(l);
                    }
                    if (l.products[0].links) {
                        arr = findAllSelected(l.products[0].links, arr);
                    }
                }
            });

            return arr;
        };
        var createExtra     = function (products, type) {
            try {
                return tmLinks.createExtraObject(tmLinks.getObjectWithType(products, 'type', type).products[0].extra);
            } catch (e) {

            }
        };
        var getExtra        = function (extras, fieldName) {
            var result = _.filter(extras, function (extra) {
                return extra.fieldName === fieldName;
            });
            return result.length ? result[0].value : '';
        };
        var getAttribute    = function (product, id) {
            var p = _.filter(product.products[0].attributes, function (extra) {
                return extra.id === id;
            });
            if (p[0] && p[0].value) {
                return p[0].value;
            }
        };

        var navigateToOffer = function (offer) {
            sbLogic.resetRatePlan();
            var toState = $state.current.name.indexOf('.pdp.') !== -1 ? $state.current.name : $state.current.data.nextState;
            $state.transitionTo(toState, createPdpParams(offer), {reload: true, inherit: false, notify: true});
        };

        var orderOffer = function (offer) {

            if (offer.origin === 'app') {
                tmSelectedProduct.setProduct(offer.selectedProductObject);
                tmReceipt.updateReceipt();
                $timeout(function(){
                    $state.go('home.ca.checkout.step-1-budget');
                    var listener = $rootScope.$on('$stateChangeSuccess', function() {
                        $rootScope.$broadcast(SB_EVENT.closeModal);
                        listener();
                    });

                });
            } else {
                var listener = $rootScope.$on(TM_RATE_PLAN.updatePrice, function () {
                    $timeout(function () {
                        $state.go('home.ca.checkout.step-1-budget');
                        var listener2 = $rootScope.$on('$stateChangeSuccess', function() {
                            $rootScope.$broadcast(SB_EVENT.closeModal);
                            listener2();
                        });
                        listener();
                    }, 800);

                });
                navigateToOffer(offer);
            }
        };

        var createOfferXML = function (offers) {
            var XML        = {
                    basket: {
                        customercode: sbUser.getInfo('code').substr(0, 10),
                        MSISDN: sbUser.getInfo('phone'),
                        emailAddress: sbUser.getInfo('email'),
                        salesAgentCode: '0000099151',
                        salesChannel: 'ESLS'
                    }
                },
                offerArray = [];

            _.forEach(offers, function (offer, i) {

                var result = _.chain(offer.selected)
                    .filter(function (product) {
                        return product.type !== 'color' && product.type !== 'HCL' && product.type !== 'SIM';
                    })
                    .map(function (product) {
                        var obj         = {};
                        obj.art_bbid = getExtra(product.products[0].extra, 'externalId');
                        obj.art_custom1 = product.type === 'AB' ? 'A' : 'O';
                        if (product.type === 'AB') {

                            obj.ab_agexport = 'RATEPLAN';
                        } else {
                            obj.ab_agexport = 'SERVICE';
                        }
                        var term     = getAttribute(product, 'abotermy') * 12;
                        term     = isNaN(term) ? '' : term;
                        obj.abotermy = term;
                        return obj;
                    }).value();

                var device = offers[i].raw;

                var obj         = {};
                obj.art_bbid    = getExtra(device.extra, 'externalId');
                obj.ab_agexport = getExtra(device.extra, 'art_issimonly') === 'TRUE' ? 'SIM' : 'HANDSET';
                obj.art_custom1 = '';
                obj.abotermy    = '';
                result.unshift(obj);

                var ol = {offerline: result};
                offerArray.push(ol);
            });

            XML.basket.offers = {offer: offerArray};

            return x2js.json2xml_str(XML);
        };

        var createOfferObject = function (offerXML, addOfferFunc) {

            var offer, receipt;

            var defer = $q.defer();

            if (!offerXML) {

                var o = {};


                offer                   = window.deepCopy(tmSelectedProduct.getProduct());
                o.selectedProductObject = window.deepCopy(offer);

                receipt = window.deepCopy(tmReceipt.getReceipt());

                o.receipt = receipt;
                o.origin  = 'app';
                o.id      = offer.id;
                o.title   = offer.supplier + ' ' + offer.name;

                o.image = offer.image.data;
                if (o.image.indexOf('http') === -1) {
                    o.image = SB_PATH.url + o.image;
                }

                o.selected  = findAllSelected(offer.links, []);
                o.minutes   = createExtra(o.selected, 'MIN');
                o.data      = createExtra(o.selected, 'DAT');
                o.rateplan  = createExtra(o.selected, 'AB');
                o.insurance = _.filter(o.selected, function (p) {
                    return p.type === 'INS';
                });
                o.raw       = offer;
                defer.resolve(o);

            }
            else if (offerXML.basket.response.result.resultCode !== '0') {

                defer.reject('No result from TMNL');

            }
            else if (offerXML.basket) {

                var promises = [];

                _.forEach(offerXML.basket.offers, function (offer) {

                    var o = {};

                    o.id = findOfferLineWithField(offer, 'ab_agexport', 'HANDSET', 'art_id');
                    if (!o.id) {
                        o.id = findOfferLineWithField(offer, 'ab_agexport', 'SIM', 'art_id');
                    }

                    if (!o.id) {
                        o.error = {
                            id: 1,
                            internalMessage: 'No tangible product found'
                        };
                        addOfferFunc(o);
                        return;
                    }

                    o.configObj = _.chain(offer.offerline)
                        .filter(function (offerline) {
                            return offerline.ab_agexport === 'SERVICE' || offerline.ab_agexport === 'RATEPLAN';
                        })
                        .map(function (offerline) {
                            return {
                                id: offerline.art_id,
                                available: offerline.available === 'True',
                                offerline: offerline
                            };
                        }).value();

                    o.config    = _.map(_.filter(o.configObj, 'available'), 'id').join(',');
                    o.pdpConfig = _.map(_.filter(o.configObj, 'available'), function (conf) {
                        return conf.offerline.base_article ? conf.offerline.base_article : conf.id;
                    }).join(',');

                    promises.push(sbResources.getFullProduct({
                        id: o.id,
                        config: o.config
                    }).then(function (response) {
                        var product = response[0];

                        if (!product) {

                            o.error = {
                                id: 2,
                                internalMessage: 'No product found'
                            };
                            addOfferFunc(o);
                            return;

                        }

                        try {

                            tmRatePlan.setZeroPriceItems(product);
                            o.image = product.image.data;
                            if (o.image.indexOf('http') === -1) {
                                o.image = SB_PATH.url + o.image;
                            }
                            o.offerID = offer.offerID;
                            o.retrieveRsponse = offer;
                            o.raw               = product;
                            o.title             = o.raw.supplier + ' ' + o.raw.name;
                            o.origin            = 'xml';
                            o.rateplan          = findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'art_id');
                            o.rateplanAvailable = findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'available') === 'True';
                            o.addOns            = [];

                            var ab         = tmLinks.getObjectWithType(product.links, 'type', 'AB');
                            var ins        = tmLinks.getObjectWithType(product.links, 'type', 'INS');
                            //var selectedAb = _.find(ab.products, function (rateplan) {
                            //    return rateplan.id === o.rateplan;
                            //});

                            o.ab = tmLinks.setDefaultSelection(ab);
                            //tmLinks.setDefaultSelection(o.ab);

                            _.each(o.ab.links, function (link) {
                                //console.log(link);
                                tmLinks.setDefaultSelection(link, null, o.config);
                            });

                            var copyProduct = window.deepCopy(tmSelectedProduct.getProduct());

                            var cp = window.deepCopy(tmSelectedProduct.setDefaults(window.deepCopy(product), null, o.config));
                            _.some(cp.links, function (link) {
                                if (link.type === 'AB' && link.products && link.products[0]) {
                                    link.products[0] = o.ab;
                                }
                            });

                            o.selectedProductObject = cp;
                            o.selected  = findAllSelected(cp.links, []);

                            tmSelectedProduct.setProduct(copyProduct);

                            var cpAb = tmLinks.getObjectWithType(cp.links, 'type', 'AB');

                            _.each(cpAb.products[0].links, function (link) {
                                link.products = _.filter(link.products, 'selected');
                            });

                            var pricing = tmPdp.createPricingObject(cp, tmRatePlanPricing.getPricing(o.ab));

                            _.map(pricing, function (price) {
                                if (isNaN(price)) {
                                    return null;
                                }
                                else {
                                    return price;
                                }
                            });

                            o.rateplan           = tmLinks.createInfoObject(o.ab);
                            o.rateplan.duration  = findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'abotermy');
                            o.rateplan.available = o.rateplanAvailable;

                            tmPricing.calcPricing(pricing).then(function (p) {
                                o.receipt                            = {};
                                o.receipt.finalPricing               = p;
                                o.selectedProductObject.finalPricing = p;
                                o.selectedProductObject.finalPricing.subscriptionDuration = o.rateplan.duration;
                            });

                            _.each(o.configObj, function (conf) {

                                var art = conf.id;

                                var match = _.filter(ins.products, function (p) {
                                    return p.id === art;
                                });

                                if (match.length) {
                                    o.insurance            = match[0];
                                    o.insurance.available  = conf.available;
                                    o.insurance.monthlyFee = parseFloat(getExtra(o.insurance.extra, 'monthlyFee'));
                                }
                                _.each(o.ab.links, function (link) {

                                    var match = _.filter(link.products, function (prod) {
                                        return prod.id === art;
                                    });

                                    if (match.length) {
                                        o[link.type] = match[0];
                                        if (link.type.indexOf('X') === 0 || link.type === 'SDP') {
                                            o.addOns.push({
                                                name: match[0].description,
                                                available: conf.available
                                            });
                                        }
                                    }

                                });
                            });
                            if (o.DAT) {
                                o.data = tmLinks.createExtraObject(o.DAT.extra);
                            }
                            if (o.MIN) {
                                o.minutes = tmLinks.createExtraObject(o.MIN.extra);
                            }

                            //console.log('offer:', o);

                            addOfferFunc(o);

                        } catch (e) {
                            o.error = {
                                id: 3,
                                internalMessage: 'Something went wrong parsing the offer!',
                                errorMessage: e
                            };
                            addOfferFunc(o);

                        }
                    }));
                });
                $q.all(promises).then(function() {
                    defer.resolve();
                });

            }
            else {
                defer.reject('Something went wrong parsing the offers');
            }
            return defer.promise;
        };

        var createRetrieveXML = function () {
            var XML = {
                basket: {
                    customercode: sbUser.getInfo('code').substr(0, 10),
                    MSISDN: sbUser.getInfo('phone'),
                    cust_fc: sbUser.getInfo('value'),
                    cust_email: sbUser.getInfo('email')
                }
            };
            return x2js.json2xml_str(XML);
        };

        /// function to pare after a retrieve //
        function findOfferLineWithField(offer, field, value, f) {

            var result = _.find(offer.offerline, function (offerLine) {
                return offerLine[field] === value;
            });
            if (result && f) {
                return result[f];
            } else {
                return result;
            }

        }

        var offerAvailable = function () {
            return !!tmSelectedProduct.getProduct();
        };

        function getSelectedProductObject() {

            if (offerAvailable()) {
                var p      = tmSelectedProduct.getProduct();
                var config = createConfig(findAllSelected(p.links));

                //var c = createConfig(findAllSelected(p.links));

                var obj = {
                    id: p.id,
                    description: tmDescription.getUrlDescription(p)
                };
                if (config) {
                    obj.config = config;
                }

                return obj;
            }

            return false;

        }

        return {
            offerAvailable: offerAvailable,
            createOfferObject: createOfferObject,
            pdpParams: createPdpParams,
            navigateToOffer: navigateToOffer,
            orderOffer: orderOffer,
            createOfferXML: createOfferXML,
            createRetrieveXML: createRetrieveXML,
            getSelectedProductObject: getSelectedProductObject
        };

    }
})();
(function () {

    'use strict';

    angular.module('SaveBasket.Offers')
        .factory('sbOffers', SaveBasketOffersService);

    //SaveBasketOffersService.$inject = ['sbOfferParser', 'sbResources'];

    function SaveBasketOffersService(sbOfferParser, sbResources, x2js, $rootScope, SB_EVENT, SB_ENV, SB_PERSISTENCE, sbSessionPersistence) {

        var offers         = [],
            maxOffers      = 1,
            selectedOffer,
            busyRetrieving = false;

        var navigateToOffer = function (offer) {
            sbOfferParser.navigateToOffer(offer);
        };

        var orderOffer = function (offer) {
            sbOfferParser.orderOffer(offer);
        };

        var compareOffer  = function (offer1, offer2) {
            return JSON.stringify(offer1.selected) === JSON.stringify(offer2.selected);
        };
        var compareOffers = function (offer) {
            var i = -1;
            _.some(offers, function (o, index) {
                if (compareOffer(o, offer)) {
                    i = index;
                    return true;
                }
            });
            return i;
        };

        var setSelectedOffer   = function (offer, persist) {
            persist = persist === undefined ? true : persist;

            $rootScope.$broadcast(SB_EVENT.changeOffer, offer);
            selectedOffer = offer;

            if (persist) {
                sbSessionPersistence.setItem(SB_PERSISTENCE.selectedOffer, selectedOffer);
            }
        };
        var getSelectedOffer   = function () {
            return selectedOffer;
        };
        var resetSelectedOffer = function () {
            $rootScope.$broadcast(SB_EVENT.changeOffer, null);
            selectedOffer = null;
            sbSessionPersistence.removeItem(SB_PERSISTENCE.selectedOffer);
        };

        var getOfferCount = function () {
            return offers.length;
        };

        /**
         * Delete the current offer in the wish list and broadcast deleteOffer event
         * @param {object} offer - Object containing current offer details.
         */
        var deleteOffer = function (offer) {
            $rootScope.$broadcast(SB_EVENT.deleteOffer, offer);

            if (selectedOffer && compareOffer(offer, selectedOffer)) {
                resetSelectedOffer();
            }
            offers.splice(offers.indexOf(offer), 1);

            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);
        };

        var addOffer = function (offer) {

            offers[0] = offer;
            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);

            $rootScope.$broadcast(SB_EVENT.addOffer, offer);
        };

        /**
         * Saves the offer in the wish list and broadcast events
         * @param {object} offer - Object containing offer details.
         */
        var _saveOffer = function (offer) {
            // If there is already an offer, it will be replaced with a new one;
            // therefore, broadcast the deleteOffer event with the previous offer as arg
            if(offers[0]) {
                $rootScope.$broadcast(SB_EVENT.deleteOffer, offers[0]);
            }

            // Assign the new offer and broadcast the saveOffer event with the new offer as arg
            offers[0] = offer;
            sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);

            sbResources.save(sbOfferParser.createOfferXML(offers)).then(
                function success(data) {
                    try {
                        offer.offerID = data.response.results.result.offerID;
                    } catch (e) {
                        offer.offerID = undefined;
                        console.warn('no offerID available :', e);
                    }
                    offer.saveResponse = data;
                    offers[0] = offer;
                    sbSessionPersistence.setItem(SB_PERSISTENCE.offers, offers);
                    $rootScope.$broadcast(SB_EVENT.saveOffer, offer);
                    resetSelectedOffer();
                },
                function error(reason) {
                    console.error('Problems saving:', reason);
                }
            );

        };

        var saveOffer = function () {

            if (!sbOfferParser.offerAvailable()) {
                return;
            }

            sbOfferParser.createOfferObject().then(
                function (offer) {

                    if (busyRetrieving) {
                        var listener = $rootScope.$on(SB_EVENT.offersRetrieved, function () {
                            _saveOffer(offer);
                            listener();
                        });
                    } else {
                        _saveOffer(offer);
                    }
                }
            );
        };

        var retrieveOffers = function () {

            busyRetrieving = true;
            sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, true);

            sbResources.retrieve(sbOfferParser.createRetrieveXML())
                .then(function (data) {
                    var xml = x2js.xml_str2json(data);
                    if (xml) {
                        sbOfferParser.createOfferObject(xml, addOffer).then(
                            function success() {
                                $rootScope.$broadcast(SB_EVENT.offersRetrieved);
                                busyRetrieving = false;
                                sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, false);
                            }, function error(e) {
                                $rootScope.$broadcast(SB_EVENT.offersRetrieved);
                                console.debug(e);
                                busyRetrieving = false;
                                sbSessionPersistence.setItem(SB_PERSISTENCE.busyRetrieving, false);
                            });
                    }
                });
        };

        function initDesktop() {
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.busyRetrieving)) {
                busyRetrieving = sbSessionPersistence.getItem(SB_PERSISTENCE.busyRetrieving);
            }


            if (busyRetrieving) {
                retrieveOffers();
            }

            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.offers)) {
                offers = sbSessionPersistence.getItem(SB_PERSISTENCE.offers);
            }
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.selectedOffer)) {
                setSelectedOffer(sbSessionPersistence.getItem(SB_PERSISTENCE.selectedOffer), false);
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        return {
            offerAvailable: sbOfferParser.offerAvailable,
            getOfferCount: getOfferCount,
            getMaxOfferCount: function () {
                return maxOffers;
            },
            saveOffer: saveOffer,
            retrieveOffers: retrieveOffers,
            compareOffers: compareOffers,
            deleteOffer: deleteOffer,
            offers: offers,
            setSelectedOffer: setSelectedOffer,
            getSelectedOffer: getSelectedOffer,
            hasSelectedOffer: function () {
                return !!getSelectedOffer();
            },
            resetSelectedOffer: resetSelectedOffer,
            getPdpParams: sbOfferParser.pdpParams,
            navigateToOffer: navigateToOffer,
            orderOffer: orderOffer
        };
    }

})();

(function () {

    'use strict';

    angular.module('SaveBasket.Offers')
        .directive('sbOfferDisplay', SaveBasketOfferDisplayDirective);

    function SaveBasketOfferDisplayDirective(SB_PATH) {
        return {

            restrict: 'E',
            scope: {
                offer: '=',
                path: '='
            },
            controller: SaveBasketOfferDisplayController,
            templateUrl: SB_PATH.templates + 'offers/templates/sbOfferDisplay.tpl.html'

        };
    }

    function SaveBasketOfferDisplayController($scope, SaveBasket) {
        $scope.deleteOffer = function(offer) {
            SaveBasket.deleteOffer(offer);
        };
        $scope.orderOffer  = function(offer) {
            SaveBasket.orderOffer(offer);
        };
        $scope.changeOffer = function(offer) {
            SaveBasket.changeOffer(offer);
        };
        $scope.isInfiniteSymbol = function (symbol) {
            var infinity = ['&#8734;', '&infin;', '∞', '&#x221E;'];
            
            return infinity.indexOf(symbol) > -1;
        };
    }



})();
(function () {

    'use strict';

    angular.module('SaveBasket.Session.Persistence', ['ngCookies']).factory('sbSessionPersistence', SaveBasketSessionPersistenceService);
    /**
     * @ngdoc service
     * @name sbSessionPersistence
     *
     * @description
     * Temporarily persists objects in a value-key map
     */
    function SaveBasketSessionPersistenceService(SB_ENV) {
        var cookieName = 'sbSessionStorage',
            hashMap    = [];

        function init() {
            hashMap = JSON.parse(sessionStorage.getItem(cookieName));
            if (!angular.isArray(hashMap)) {
                hashMap = [];
            }
        }

        function clearSession() {
            sessionStorage.setItem(cookieName, '');
        }

        function update() {
            sessionStorage.setItem(cookieName, JSON.stringify (hashMap));
        }

        if (SB_ENV.isDesktop) {
            init();
            window.onbeforeunload = function () {
                //sessionStorage.setItem(cookieName, JSON.stringify (hashMap));
            };
        }

        function getItem(key) {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                result,
                i;
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k === key) {
                    result = item.v;
                }
            }
            return result;
        }

        function setItem(key, data) {
            if (SB_ENV.isMobile) {
                return;
            }
            removeItem(key);
            hashMap.push({k: key, v: data});
            update();
        }

        function removeItem(key) {
            if (SB_ENV.isMobile) {
                return;
            }
            var item,
                i,
                _hashMap = [];
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k !== key) {
                    _hashMap.push(item);
                }
            }
            hashMap = _hashMap;
            update();
        }

        function getAllItems() {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                i,
                result = [];
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                result.push(item.v);
            }
            return result;
        }

        function getAllItemsWithKeys() {
            if (SB_ENV.isMobile) {
                return false;
            }
            return hashMap;
        }

        function clearItems() {
            if (SB_ENV.isMobile) {
                return false;
            }
            hashMap = [];
            update();
        }

        function itemExists(key) {
            if (SB_ENV.isMobile) {
                return false;
            }
            var item,
                i,
                result = false;
            for (i = 0; i < hashMap.length; i++) {
                item = hashMap[i];
                if (item.k === key) {
                    result = true;
                    break;
                }
            }
            return result;
        }

        return {
            setItem: setItem,
            getItem: getItem,
            removeItem: removeItem,
            getAllItems: getAllItems,
            getAllItemsWithKeys: getAllItemsWithKeys,
            clearItems: clearItems,
            itemExists: itemExists,
            clear: clearSession
        };
    }
})();
(function () {

    'use strict';

    /**
     * @module SaveBasket.Resources
     * @description Services for retrieving and saving offers and
     * getting a product json for the product in a offer
     */



    angular.module('SaveBasket.Resources')
        .factory('sbResources', SaveBasketResourcesService);

    function SaveBasketResourcesService($http, SB_PATH, $q, ProductResource, x2js) {

        /**
         * Saves an offer
         *
         * @param offerXML
         * @returns {*|r.promise|promise|m.promise|{then, catch, finally}}
         */
        var save = function (offerXML) {

            var defer = $q.defer();

            $http({
                method: 'post',
                data: offerXML,
                headers: {
                    'Content-Type': 'application/XML'
                },
                url: SB_PATH.url + 'service/basket/save'
            }).then(
                function succes(response) {
                    defer.resolve(x2js.xml_str2json(response.data));
                },
                function error(reason) {
                    defer.reject(reason);
                }
            );

            return defer.promise;

        };

        var retrieve = function (xml) {

            var defer = $q.defer();

            $http({
                method: 'post',
                data: xml,
                headers: {
                    'Content-Type': 'application/XML'
                },
                url: SB_PATH.url + 'service/basket/get'
            }).then(
                function succes(response) {
                    defer.resolve(response.data);
                },
                function error(reason) {
                    defer.reject(reason);
                }
            );

            return defer.promise;
        };

        /** Retrieve the full json object for a given product **/
        var getFullProduct = function (params) {

            return ProductResource.get(params).$promise;

        };

        return {
            save: save,
            retrieve: retrieve,
            getFullProduct: getFullProduct
        };

    }

})();
(function () {
    'use strict';

    angular.module('SaveBasket.Trigger')
        .directive('sbOfferCount', saveBasketOfferCountDirective);

    function saveBasketOfferCountDirective(SaveBasket) {
        return {
            restrict: 'A',
            link: function(scope, element) {

                setAttribute(SaveBasket.offerCount());

                scope.$watch(function() {
                    return SaveBasket.offerCount();
                }, function(val) {
                    setAttribute(val);
                });

                function setAttribute(nval, oval) {
                    if (nval !== oval) {
                        if (nval) {
                            element.attr('sb-offer-count-value', nval);
                        } else {
                            element.removeAttr('sb-offer-count-value');
                        }

                    }
                }

            }
        };
    }

})();
(function () {

    'use strict';

    angular.module('SaveBasket.Trigger')
        .directive('sbTrigger', saveBasketTriggerDirective);

    function saveBasketTriggerDirective(SaveBasket, sbModal, $rootScope, SB_EVENT) {
        return {
            restrict: 'EA',
            scope: {
                command: '@sbTrigger',
                openBasket: '=openBasket',
                addText: '@',
                changeText: '@'
            },
            controller: function($rootScope) {
                //ToDo - find a better solution! - Used for configurable text on trigger link.
                $rootScope.addOffer = !SaveBasket.hasSelectedOffer();
                $rootScope.changeOffer = SaveBasket.hasSelectedOffer();
            },
            link: function (scope, element) {

                element.removeClass('hidden');

                if (SaveBasket.hasSelectedOffer()) {
                    setText(scope.changeText);
                } else {
                    setText(scope.addText);
                }

                $rootScope.$on(SB_EVENT.changeOffer, function(event, offer) {

                    if (offer) {
                        setText(scope.changeText);
                    } else {
                        setText(scope.addText);
                    }
                });

                $rootScope.$on(SB_EVENT.showBasket, function(event, show) {
                    if (show) {
                        element.removeClass('hidden');
                    } else {
                        element.addClass('hidden');
                    }
                });

                element.on('click', function () {

                    $rootScope.$broadcast(SB_EVENT.currentCommand, scope.command);

                    if (typeof scope.openBasket === 'undefined' || scope.openBasket) {
                        sbModal.openBasket(scope.command);
                    }

                });

                function setText(text) {
                    if (text) {
                        element.find('.sb-trigger-text').text(text);
                    }

                }
            }
        };
    }

})();
(function () {

    'use strict';


    angular.module('SaveBasket.User')
        .factory('sbUserLogin', SaveBasketUserLoginService);

    /**
     * @ngdocs service
     * @name sbUserLogin
     * @description Handles the login for a ECA customer. The login for a ECR customer is handled the normal way by the application
     * If a cryptoticket is available on ECA, it means that the customer is redirected back from the Portals verification.
     */
    function SaveBasketUserLoginService($location, SB_PATH, SB_EVENT, sbUser, tmAuthorization, $rootScope, $http, $window, sbOfferParser, $state, SB_PORTALS_LOGIN) {

        var currentCommand = '';
        $rootScope.$on(SB_EVENT.currentCommand, function(event, command) {
            currentCommand = command;
        });

        function validateCrypto() {
            var
                params = $location.search(),
                credentials = {
                    user: params.ct,
                    password: params.sp
                },
                url = SB_PATH.loginUrl + 'session/logIn?user=' + $window.encodeURIComponent(credentials.user) + '&password=' + $window.encodeURIComponent(credentials.password)
                ;

            //Broadcast that login started with the credentials
            //$rootScope.$broadcast(TM_AUTH_EVENTS.loginStarted, {credentials: credentials});

            $http({method: 'GET', url: url, withCredentials: true, data: credentials, cache: true})
                .then(
                function (authorization) {

                    try {
                        if (authorization.data.extra['customer.code'] && (!authorization.data.extra.hasOwnProperty('elegible') ||authorization.data.extra.elegible === '1')) {
                            // user needs to be redirected to ecr..
                            window.location = window.location.href.replace('-ca', '-cr').replace('/eca/', '/ecr/');
                            return;
                        }

                        if (!authorization.data.username) {
                            $rootScope.$broadcast(SB_EVENT.userValidationFailed);
                            return;
                        }
                        sbUser.set({
                            validation: {
                                extra : {
                                    'customer.phone': '',
                                    'customer.code': '',
                                    'customer.email': authorization.data.username
                                }
                        }
                        });
                    }
                    catch(e) {

                        $rootScope.$broadcast(SB_EVENT.userValidationFailed, e);
                    }

                },
                function (error) {
                    //login failed state
                    console.log('error: ', error);
                    //$rootScope.$broadcast(TM_AUTH_EVENTS.loginFailed, {error: error});
                    //TODO ask patrick to see if feedback is needed
                });

        }

        /**
         * Use the functions from the normal app the verify if there is a crypto available
         *
         * If there is, validate it
         */
        function checkForCrypto() {
            if (tmAuthorization.isCryptoAvailable()) {
                validateCrypto();
            }
        }

        /**
         * On the first $stateChangeSuccess check for a crypto and de-register the event listener
         **/
        var firstStateChange = $rootScope.$on('$stateChangeSuccess', function (event, state) {
            if (state.name.indexOf('.cr.') === -1) {
                checkForCrypto();
            }
            firstStateChange();
        });

        function login() {

            window.location = SB_PORTALS_LOGIN.url + generateRedirectUrl();

        }

        function generateRedirectUrl() {

            var url = window.location.origin + window.location.pathname,
                obj = sbOfferParser.getSelectedProductObject();

            if (!obj) {
                return encodeURIComponent(url);

            } else {

                if ($state.current.name.indexOf('.cr.') !== -1) {
                    url = window.location.origin + '/ecr/pdp/' + obj.description + '/' + obj.id;
                } else {
                    url = window.location.origin + '/eca/pdp/' + obj.description + '/' + obj.id;
                }
                if (obj.config) {
                    url += '?config=' + obj.config;
                }

                if (currentCommand === 'save') {
                    url += '&savebasket=true';
                }

                return encodeURIComponent(url);
            }

        }
        $rootScope.$on(SB_EVENT.validateUser, function() {
            login();
        });


        return {
            login: login
        };

    }

})();
(function () {

    'use strict';

    angular.module('SaveBasket.User')
        .service('sbUser', SaveBasketUserService);

    function SaveBasketUserService($rootScope, SB_EVENT, SB_PERSISTENCE, sbSessionPersistence, SB_ENV, sbResources, $timeout, tmChannel) {
        var user                = {},
            userValidated       = false;

        function initDesktop() {
            if(sbSessionPersistence.itemExists( SB_PERSISTENCE.user )){
                user = sbSessionPersistence.getItem( SB_PERSISTENCE.user );
                set(user, false);
            } else {
                sbResources.getUser().then(
                    function succes(user) {
                        user = user.mdorcust.tt_customer[0];

                        var userObj = {
                            validation: {
                                extra: {
                                    'customer.code'   : user.customerCode,
                                    'customer.phone'  : user.cust_msisdn,
                                    'customer.email'  : user.cust_email,
                                    'customer.value'  : user.cust_fc,
                                    'customer.value2' : user.cust_fc2
                                }
                            }
                        };
                        set(userObj);
                    },
                    function error(reason) {
                        console.log('Failed to retrieve the user', reason);
                    });
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        function get() {
            return user;
        }

        function set(u, persist) {

            tmChannel.channelInfo().then(
                function success(channel) {

                    if (channel.extra.enable_savebasket) {
                        persist = persist === undefined ? true : persist;

                        // validate if the user is really known, mambo sends a user object even if the user is not know at all
                        if (!getInfo('code', u) && !getInfo('email', u) && !getInfo('msisdn', u)) {
                            return;
                        }

                        user = u;

                        if (!userValidated) {
                            userValidated = true;
                            $timeout(function(){
                                $rootScope.$broadcast(SB_EVENT.userValidated, u, persist);
                            });

                        }
                        if(persist){
                            sbSessionPersistence.setItem(SB_PERSISTENCE.user, u);
                        }
                    }
                }
            );
        }

        function getInfo(field, userObj) {

            var info = '';
            userObj = userObj || user;


            try {
                info = userObj.validation.extra['customer.' + field] || '';
            } catch (e) {

            }

            return info;

        }


        return {
            get: get,
            set: set,
            getInfo: getInfo
        };
    }


})();