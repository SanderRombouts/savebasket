(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer')
        .factory('sbDataLayerParser', SaveBasketDataLayerParser);

    function SaveBasketDataLayerParser(googleTagDayaLayer, $q) {

        function create(offer) {

            return fillPackage(offer);
        }

        function addOfferID(packageObj, offer) {
            packageObj[0].OfferID = offer.offerID;
            return packageObj;
        }

        function fillPackage(offer) {

            var defer = $q.defer();

            var products = offer.selected.map(function (item) {
                return item.type !== 'color' && item.type !== 'AB-SWITCH' && item.products.length ? item.products[0] : false;
            }).filter(function (item) {
                return item.id && item.id !== 'zero-item';
            });

            products[0].finalPricing = offer.selectedProductObject.finalPricing;


            var packageObj = googleTagDayaLayer.createPackageObject(products);

            packageObj = addOfferID(packageObj, offer);

            defer.resolve(packageObj);

            return defer.promise;
        }

        return {
            create: create
        }
    }
})();
