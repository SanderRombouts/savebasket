(function () {
    'use strict';

    angular.module('SaveBasket.DataLayer')
        .factory('sbDataLayerParser', SaveBasketDataLayerParser);

    function SaveBasketDataLayerParser(sbResources, $q) {

        function create(offer) {
            //TODO (created 28/04/16 14:59 by sanderrombouts) Priority: High - make sure offer.offerID exists #TODO

            return fillPackage(offer);
        }

        function addOfferID(packageObj, offer) {
            //TODO (created 28/04/16 14:55 by sanderrombouts) Priority: High - make sure offer.offerID exists #TODO
            packageObj[0].OfferID = offer.offerID;
            return packageObj;
        }

        function fillPackage(offer) {

            var defer = $q.defer(),
                options = {
                    art_id: offer.raw.globals[0].artId,
                    abo_id: offer.raw.globals[0].aboId,
                    xart_id: offer.raw.globals[0].combishow
                };

            sbResources.getDataLayer(options).then(
                function success(response) {
                    var data = mapNullToUndefined(response.data.mdodlpix.Shop.Packages);

                    data = addOfferID(data, offer);

                    defer.resolve(data);
                },
                function error(reason) {
                    defer.reject(reason)
                }
            );

            return defer.promise;
        }

        function mapNullToUndefined(data) {

            var obj;

            if (data instanceof Array) {
                obj = []
            } else if (typeof data === 'object') {
                obj = {}
            }

            _.each(data, function(val, key) {

                if (data[key] !== null && typeof data[key] !== 'string') {
                    obj[key] = mapNullToUndefined(val);
                } else {
                    obj[key] = val === null ? undefined : val;
                }
            });

            return obj;

        }

        return {
            create: create
        };
    }
})();