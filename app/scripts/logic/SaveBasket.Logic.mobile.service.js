(function () {

    'use strict';

    angular.module('SaveBasket.Logic')
        .factory('sbLogic', SaveBasketLogicService);

    //SaveBasketLogicService.$inject = ['$rootScope', 'SB_EVENT', 'tmProduct'];

    function SaveBasketLogicService($rootScope, SB_EVENT, tmProduct, Params, tmChannel) {

        var showBasket  = false;
        var channelInfo = {};

        $rootScope.$on(tmChannel.currentChannelChangeEvent, function (e, channel) {
            channelInfo = channel;
        });

        $rootScope.$on('$viewContentLoaded', function (event) {

            tmChannel.channelInfo().then(
                function success(channel) {

                    channelInfo = channel;

                    try {
                        showBasket = event.currentScope.$state.current.data.showSaveBasket && channel.extra.enable_savebasket;
                    } catch (e) {
                        /**
                         * Should be false, but on order to always show the triggers!!
                         * @type {boolean}
                         */
                        showBasket = false;
                    }

                    $rootScope.$broadcast(SB_EVENT.showBasket, showBasket);
                }
            );

        });

        function resetRatePlan() {
            tmProduct.ratePlan.resetRatePlan();
            tmProduct.hip.reset();
            Params.reset();
        }

        return {
            resetRatePlan: resetRatePlan,
            showBasket: showBasket,
            channelInfo: channelInfo
        };
    }

})();