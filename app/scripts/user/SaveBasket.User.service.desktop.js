(function () {

    'use strict';

    angular.module('SaveBasket.User')
        .service('sbUser', SaveBasketUserService);

    function SaveBasketUserService($rootScope, SB_EVENT, SB_PERSISTENCE, sbSessionPersistence, SB_ENV, sbResources, $timeout) {
        var user          = {},
            userValidated = false;

        function initDesktop() {
            if (sbSessionPersistence.itemExists(SB_PERSISTENCE.user)) {
                user = sbSessionPersistence.getItem(SB_PERSISTENCE.user);
                set(user, false);
            } else {
                sbResources.getUser().then(
                    function succes(user) {
                        user = user.mdorcust.tt_customer[0];

                        var userObj = {
                            validation: {
                                extra: {
                                    'customer.code': user.customerCode,
                                    'customer.phone': user.cust_msisdn,
                                    'customer.email': user.cust_email,
                                    'customer.value': user.cust_fc,
                                    'customer.value2': user.cust_fc2
                                }
                            }
                        };
                        set(userObj);
                    },
                    function error(reason) {
                        console.log('Failed to retrieve the user', reason);
                    });
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        function get() {
            return user;
        }

        function set(u, persist) {

            persist = persist === undefined ? true : persist;

            // validate if the user is really known, mambo sends a user object even if the user is not know at all
            if (!getInfo('code', u) && !getInfo('email', u) && !getInfo('msisdn', u)) {
                return;
            }

            user = u;

            if (!userValidated) {
                userValidated = true;
                $timeout(function () {
                    $rootScope.$broadcast(SB_EVENT.userValidated, u, persist);
                });

            }
            if (persist) {
                sbSessionPersistence.setItem(SB_PERSISTENCE.user, u);
            }
        }

        function getInfo(field, userObj) {

            var info = '';
            userObj  = userObj || user;

            try {
                info = userObj.validation.extra['customer.' + field] || '';
            } catch (e) {

            }

            return info;

        }

        return {
            get: get,
            set: set,
            getInfo: getInfo
        };
    }

})();