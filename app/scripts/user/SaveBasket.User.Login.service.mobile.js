(function () {

    'use strict';


    angular.module('SaveBasket.User')
        .factory('sbUserLogin', SaveBasketUserLoginService);

    /**
     * @ngdocs service
     * @name sbUserLogin
     * @description Handles the login for a ECA customer. The login for a ECR customer is handled the normal way by the application
     * If a cryptoticket is available on ECA, it means that the customer is redirected back from the Portals verification.
     */
    function SaveBasketUserLoginService($location, SB_PATH, SB_EVENT, sbUser, tmAuthorization, $rootScope, $http, $window, sbOfferParser, $state, SB_PORTALS_LOGIN) {

        var currentCommand = '';
        $rootScope.$on(SB_EVENT.currentCommand, function(event, command) {
            currentCommand = command;
        });

        function validateCrypto() {
            var
                params = $location.search(),
                credentials = {
                    user: params.ct,
                    password: params.sp
                },
                url = SB_PATH.loginUrl + 'session/logIn?user=' + $window.encodeURIComponent(credentials.user) + '&password=' + $window.encodeURIComponent(credentials.password)
                ;

            //Broadcast that login started with the credentials
            //$rootScope.$broadcast(TM_AUTH_EVENTS.loginStarted, {credentials: credentials});

            $http({method: 'GET', url: url, withCredentials: true, data: credentials, cache: true})
                .then(
                function (authorization) {

                    try {
                        if (authorization.data.extra['customer.code'] && (!authorization.data.extra.hasOwnProperty('elegible') ||authorization.data.extra.elegible === '1')) {
                            // user needs to be redirected to ecr..
                            window.location = window.location.href.replace('-ca', '-cr').replace('/eca/', '/ecr/');
                            return;
                        }

                        if (!authorization.data.username) {
                            $rootScope.$broadcast(SB_EVENT.userValidationFailed);
                            return;
                        }
                        sbUser.set({
                            validation: {
                                extra : {
                                    'customer.phone': '',
                                    'customer.code': '',
                                    'customer.email': authorization.data.username
                                }
                        }
                        });
                    }
                    catch(e) {

                        $rootScope.$broadcast(SB_EVENT.userValidationFailed, e);
                    }

                },
                function (error) {
                    //login failed state
                    console.log('error: ', error);
                    //$rootScope.$broadcast(TM_AUTH_EVENTS.loginFailed, {error: error});
                    //TODO ask patrick to see if feedback is needed
                });

        }

        /**
         * Use the functions from the normal app the verify if there is a crypto available
         *
         * If there is, validate it
         */
        function checkForCrypto() {
            if (tmAuthorization.isCryptoAvailable()) {
                validateCrypto();
            }
        }

        /**
         * On the first $stateChangeSuccess check for a crypto and de-register the event listener
         **/
        var firstStateChange = $rootScope.$on('$stateChangeSuccess', function (event, state) {
            if (state.name.indexOf('.cr.') === -1) {
                checkForCrypto();
            }
            firstStateChange();
        });

        function login() {

            window.location = SB_PORTALS_LOGIN.url + generateRedirectUrl();

        }

        function generateRedirectUrl() {

            var url = window.location.origin + window.location.pathname,
                obj = sbOfferParser.getSelectedProductObject();

            if (!obj) {
                return encodeURIComponent(url);

            } else {

                if ($state.current.name.indexOf('.cr.') !== -1) {
                    url = window.location.origin + '/ecr/pdp/' + obj.description + '/' + obj.id;
                } else {
                    url = window.location.origin + '/eca/pdp/' + obj.description + '/' + obj.id;
                }
                if (obj.config) {
                    url += '?config=' + obj.config;
                }

                if (currentCommand === 'save') {
                    url += '&savebasket=true';
                }

                return encodeURIComponent(url);
            }

        }
        $rootScope.$on(SB_EVENT.validateUser, function() {
            login();
        });


        return {
            login: login
        };

    }

})();