(function () {

    'use strict';

    angular.module('SaveBasket.Offers.Parser', [])
        .factory('sbOfferParser', SaveBasketOfferParser);

    function SaveBasketOfferParser($rootScope, x2js, SB_PATH, sbUser, sbResources, sbFormInfo, $q, sbSalesChannel, SB_EVENT) {

        var createPdpParams = function () {
            return {};
        };
        var getRatePlanAddon    = function (array, type) {
            return _.filter(array, function (addon) {
                return addon.arId.toLowerCase() === type.toLowerCase();
            });
        };
        var getFieldFromAddon   = function (addon, field) {
            return addon && addon[0] && addon[0][field] ? addon[0][field] : '';
        };
        var decimals            = function (val, num) {
            return val.toFixed(num);
        };
        var currency            = function (val) {
            return decimals(val, 2).replace('.', ',');
        };

        var parseMdorJSON = function (data, retrieveResponse) {
            var o = {},
                addon;

            if (retrieveResponse) {
                o.offerID = retrieveResponse.offerID;
                o.retrieveRsponse = retrieveResponse;
            }

            o.origin       = 'app';

            try {
                addon          = data.handset[0];
                o.id           = addon.artId;
                o.handsetTotal = parseFloat(addon.priceOnceUnformatted) > 0 ? addon.priceOnceUnformatted : '';
                o.image = addon.artImage;

                if (addon.artIsSim) {
                    o.title = data.rateplan[0].artDesc + ' Only';
                } else {
                    o.title = addon.artSupplier + ' ' + addon.artDesc;
                }
            } catch(e) {
                console.debug('No handset available');
            }
            var rateplan = data.rateplan[0];

            o.rateplan = {
                duration: rateplan.aboTermInMonths,
                showTShirtSize: rateplan.showTShirtSize
            };

            var arr = data.rateplan[0].package;

            addon  = getRatePlanAddon(arr, 'DAT');
            o.data = {
                abodat: getFieldFromAddon(addon, 'artDesc'),
                abodatdown: getFieldFromAddon(addon, 'artDatDown'),
                tshirtSize: getFieldFromAddon(addon, 'tshirtSize'),
                isUnlimited: getFieldFromAddon(addon, 'isUnlimited')
            };

            addon     = getRatePlanAddon(arr, 'MIN');
            o.minutes = {
                abobun: getFieldFromAddon(addon, 'artDesc'),
                abobmin: getFieldFromAddon(addon, 'artAbobmin'),
                suffix: getFieldFromAddon(addon, 'artAbobmin') && getFieldFromAddon(addon, 'artAbobmin').toLowerCase().indexOf('onbeperkt') === -1 ? 'Minuten' : ''
            };

            addon       = data.insurance;
            o.insurance = {
                description: getFieldFromAddon(addon, 'artDesc')
            };

            addon       = data.mbverdeler;
            o.mbverdeler = {
                description: getFieldFromAddon(addon, 'artDesc')
            };

            o.addons     = _.chain(data.addons)
                .filter(function (addOn) {
                    return addOn.arId !== 'AB';
                })
                .map(function (addon) {
                    var o  = {};
                    o.name = addon.artDesc;
                    return o;
                })
                .value();
            var totals   = data.totals[0];
            o.totalPrice = currency(parseFloat(totals.totalMonthUnformatted) + parseFloat(totals.totalInsuranceUnformatted));

            o.raw = data;

            return o;
        };

        var createOfferObject = function (offerXML, addOfferFunc) {

            var defer = $q.defer();

            if (!offerXML) {

                sbResources.getFullProduct().then(
                    function (data) {

                        if (!data.mdorjson) {
                            defer.reject('no data');
                            return defer.promise;
                        }

                        defer.resolve(parseMdorJSON(data.mdorjson));
                    },
                    function (error) {

                        defer.reject(error);
                    });

            }
            else if (offerXML.basket.response.result.resultCode !== '0') {

                defer.reject('No result from TMNL');

            }
            else if (offerXML.basket) {

                var promises = [];

                _.forEach(offerXML.basket.offers, function (offer) {

                    var params = {};
                    var o      = {};

                    o.offerID = offer.offerID;
                    o.retrieveRsponse = offer;

                    var device = findOfferLineWithField(offer, 'ab_agexport', 'HANDSET') || findOfferLineWithField(offer, 'ab_agexport', 'SIM');

                    if (!device.art_id || device.available !== 'True') {
                        o.error = {
                            id: 1,
                            internalMessage: 'No tangible product found'
                        };
                        //addOfferFunc(o);
                        return;
                    }
                    params.art_id = device.art_id;

                    o.configObj = _.chain(offer.offerline)
                        .filter(function (offerline) {
                            return offerline.ab_agexport === 'SERVICE' || offerline.ab_agexport === 'RATEPLAN';
                        })
                        .map(function (offerline) {
                            return {
                                id: offerline.art_id,
                                available: offerline.available === 'True',
                                baseArticle: offerline.base_article,
                                offerline: offerline
                            };
                        }).value();

                    params.xart_id = _.map(_.filter(o.configObj, 'available'), 'id').join(',');
                    var init_abo_id = findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'base_article') || findOfferLineWithField(offer, 'ab_agexport', 'RATEPLAN', 'art_id');
                    params.init_abo_id = init_abo_id;

                    promises.push(sbResources.getFullProduct(params).then(
                        function (data) {


                            if (!data.mdorjson) {
                                o.error = {
                                    id: 3,
                                    internalMessage: 'Something went wrong parsing the offer!',
                                    errorMessage: ''
                                };
                                addOfferFunc(o);
                            } else {
                                addOfferFunc(parseMdorJSON(data.mdorjson, offer));
                            }
                        },
                        function (e) {

                            o.error = {
                                id: 3,
                                internalMessage: 'Something went wrong parsing the offer!',
                                errorMessage: e
                            };
                            addOfferFunc(o);
                        }));
                });
                $q.all(promises).then(function() {
                    defer.resolve();
                });

            }
            else {
                defer.reject('Something went wrong parsing the offers');
            }
            return defer.promise;
        };

        /// function to pare after a retrieve //
        function findOfferLineWithField(offer, field, value, f) {

            var result = _.find(offer.offerline, function (offerLine) {
                return offerLine[field] === value;
            });
            if (result && f) {
                return result[f];
            } else {
                return result;
            }

        }

        var replaceRatePlanWithBase = function (xart, rateplan, base) {

            if (base && rateplan) {

                xart = xart.replace(rateplan, base);

            }
            return xart;

        };

        var navigateToOffer = function (offer) {

            var globals  = offer.raw.globals[0],
                rateplan = offer.raw.rateplan[0];

            var form      = sbFormInfo.getAll();
            var form_xart = _.chain(form.xart_id.split(','))
                .filter(function (product) {
                    return product.toLowerCase().indexOf('hcopy') === -1;
                })
                .sortBy()
                .value()
                .join(',');

            var offer_xart = _.chain(globals.combishow.split(','))
                .sortBy()
                .value()
                .join(',');

            var combishow = replaceRatePlanWithBase(offer_xart, rateplan.artId, rateplan.artBaseId);

            // if the offer is the same as the current PDP, just close the modal
            if (form.art_id !== globals.artId || form_xart !== offer_xart) {
                // modify the combishow to prevent lock-in. eg. replace 24brpp002 with 24brpp001 and
                // let the bestPriceCheck change it to 24brpp002 again

                window.location = encodeURI('/' + SB_PATH.channel + '/mcsmambo.p?m5nexturl=RAPRD&art_id=' + globals.artId + '&xart_id=' + combishow);
            } else {
                $rootScope.$broadcast(SB_EVENT.modalClose);
            }
        };

        var orderOffer = function (offer) {

            var query = {
                M5NextUrl: 'MDORAOPT',
                Currurl: 'RAPRD',
                art_id: offer.id,
                bag_add: offer.id,
                xart_id: offer.raw.globals[0].combishow,
                abo_id: offer.raw.globals[0].aboId,
                init_abo_id: offer.raw.rateplan[0].artBaseId
            };

            query = _.map(query, function (val, key) {
                return key + '=' + val;
            }).join('&');

            window.location = encodeURI('/' + SB_PATH.channel + '/mcsmambo.p?' + query);

        };

        var createOfferXML = function (offers) {
            var XML        = {
                    basket: {
                        customercode: sbUser.getInfo('code'),
                        MSISDN: sbUser.getInfo('phone'),
                        emailAddress: sbUser.getInfo('email'),
                        salesAgentCode: sbSalesChannel.getSalesAgent().ab_dealer,
                        salesChannel: SB_PATH.channel.indexOf('r') === 1 ? 'TSHOP' : 'ESLS'
                    }
                },
                offerArray = [];

            // create offerArray
            _.forEach(offers, function (offer) {
                var raw    = offer.raw;
                var result = [].concat(raw.rateplan, raw.handset, raw.addons, raw.hip, raw.rateplan[0].package, raw.insurance, raw.mbverdeler);

                var termy = raw.rateplan[0].aboTermInMonths;

                result = _.chain(result)
                    .compact()
                    .filter(function (item) {
                        return !!item.artId;
                    })
                    .map(function (item) {
                        var obj      = {};
                        obj.art_bbid = item.artBBID;

                        if (item.isDevice) {
                            obj.art_custom1 = '';
                            obj.ab_agexport = item.artIsSim ? 'SIM' : 'HANDSET';
                        } else if (item.isRatePlan) {
                            obj.art_custom1 = 'A';
                            obj.ab_agexport = 'RATEPLAN';
                        } else {
                            obj.art_custom1 = 'O';
                            obj.ab_agexport = 'SERVICE';
                        }
                        obj.abotermy = termy;

                        return obj;
                    })
                    .value();

                var ol = {offerline: result};
                offerArray.push(ol);
            });

            XML.basket.offers = {offer: offerArray};

            return x2js.json2xml_str(XML);
        };

        var offerAvailable = function () {

            return !!sbFormInfo.get('art_id');
        };

        function getSelectedProductObject() {

            var all = sbFormInfo.getAll(),
                xart_id = all.xart_id;

            if (all.init_abo_id && all.abo_id) {
                xart_id = xart_id.replace(all.abo_id, all.init_abo_id);
            }


            if (offerAvailable()) {
                var obj = {
                    id: sbFormInfo.get('art_id'),
                    description: sbFormInfo.get('art_desc'),
                    config: xart_id
                };

                return obj;
            }

            return false;

        }

        var createRetrieveXML = function () {
            var XML = {
                basket: {
                    customercode: sbUser.getInfo('code'),
                    MSISDN: sbUser.getInfo('phone'),
                    cust_fc: sbUser.getInfo('value'),
                    cust_fc2: sbUser.getInfo('value2'),
                    cust_email: sbUser.getInfo('email')
                }
            };
            return x2js.json2xml_str(XML);
        };

        return {
            offerAvailable: offerAvailable,
            createOfferObject: createOfferObject,
            pdpParams: createPdpParams,
            navigateToOffer: navigateToOffer,
            createOfferXML: createOfferXML,
            createRetrieveXML: createRetrieveXML,
            getSelectedProductObject: getSelectedProductObject,
            orderOffer: orderOffer
        };

    }
})();