(function () {

    'use strict';

    angular.module('SaveBasket.SalesChannel')
        .factory('sbSalesChannel', SaveBasketSalesChannelService);


    function SaveBasketSalesChannelService(sbResources, sbSessionPersistence, SB_ENV, SB_PERSISTENCE) {

        var salesAgent;

        function initDesktop() {

            if(sbSessionPersistence.itemExists( SB_PERSISTENCE.salesAgent )){
                setSalesAgent(sbSessionPersistence.getItem( SB_PERSISTENCE.salesAgent ), false);
            } else {
                sbResources.getSalesAgent().then(
                    function succes(salesAgent) {
                        setSalesAgent(salesAgent, true);
                    },
                    function error(reason) {
                        console.log('Failed to retrieve the SalesAgent', reason);
                    });
            }
        }

        if (SB_ENV.isDesktop) {
            initDesktop();
        }

        function setSalesAgent(agent, persist) {

            salesAgent = agent;

            persist = typeof persist === 'undefined' ? true : persist;

            if (persist) {
                sbSessionPersistence.setItem( SB_PERSISTENCE.salesAgent, salesAgent );
            }
        }
        function getSalesAgent() {
            return salesAgent || {};
        }



        return {

            getSalesAgent: getSalesAgent

        };
    }

})();