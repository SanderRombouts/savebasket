(function () {

    'use strict';

    angular.module('SaveBasket.Constants')

        /**
         * @ngdoc service
         * @name SaveBasket.Constants.SB_PATH
         * @description
         * Path Constants used throughout the saveBasket module
         */
        .constant('SB_PATH', {
            url: (function(){
                if (location.hostname.indexOf('tmobile') === -1) {
                    return location.protocol + '//' + location.hostname + (location.port ? ':'+location.port : '') + '/ws/';
                } else {
                    //return location.protocol + '//' + location.hostname + ':8181/';
                    return 'https://mobile-rapid.shop.t-mobile.nl/ws/';

                }
            })(),
            loginUrl: (function(){
                if (location.hostname.indexOf('tmobile') === -1) {
                    return location.protocol + '//' + location.hostname + (location.port ? ':'+location.port : '') + '/ws/';
                } else {
                    //return location.protocol + '//' + location.hostname + ':8181/';
                    return 'https://mobile-rapid.shop.t-mobile.nl/ws/';

                }
            })(),
            widget: 'scripts/widgets/save-basket/',
            templates: ''
        })
        .constant('SB_ENV', {
            platform: 'mobile',
            isDesktop: false,
            isMobile: true
        });

})();