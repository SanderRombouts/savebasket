(function () {

    'use strict';

    angular.module('SaveBasket.Constants')

        /**
         * @ngdoc service
         * @name SaveBasket.Constants.SB_PATH
         * @description
         * Path Constants used throughout the saveBasket module
         */
        .constant('SB_PATH', {
            url: (function () {
                var path = window.location.pathname;
                return path;
            })(),
            channel: (function () {
                if (window.location.origin.indexOf('verlengen') !== -1) {
                    return 'ecr';
                }
                var path = window.location.pathname;
                return path.split('/')[1] || 'eca';
            })(),
            loginUrl: (function () {
                if (location.hostname.indexOf('tmobile') === -1) {
                    return location.protocol + '//' + location.hostname + (location.port ? ':' + location.port : '') + '/ws/';
                } else {
                    return location.protocol + '//' + location.hostname + ':8181/';

                }
            })(),
            widget: '',
            templates: ''
        })
        .constant('SB_ENV', {
            platform: 'desktop',
            isDesktop: true,
            isMobile: false
        })
        .constant('SB_MAMBO_USER', {
            retrieve: '83rt' + ( window.location.pathname.split('/')[1] || 'eca' ) + 'basket',
            save: '83svbasket',
            password: 'mtvmambo5'
        });

})();