(function () {

    'use strict';


    angular.module('SaveBasket.Constants', [])


        /**
         * @ngdoc service
         * @name SaveBasket.Constants.SB_EVENTS
         * @description
         * Events used throughout the saveBasket module
         */
        .constant('SB_EVENT', {
            currentCommand      : 'SB_CURRENT_COMMAND',
            saveOffer           : 'SB_SAVE_OFFER',
            deleteOffer         : 'SB_DELETE_OFFER',
            modalClose          : 'SB_MODAL_CLOSE',
            userValidated       : 'SB_USER_VALIDATED',
            userValidationFailed: 'SB_USER_NOT_VALIDATED',
            validateUser        : 'SB_VALIDATE_USER',
            showBasket          : 'SB_SHOW_BASKET',
            changeOffer         : 'SB_CHANGE_OFFER',
            offersRetrieved     : 'SB_OFFERS_RETRIEVED',
            openBasket          : 'SB_OPEN_BASKET',
            closeModal          : 'SB_CLOSE_MODAL',
            dataLayerEvent      : 'SB_DATALAYER_EVENT'
        })
        .constant('SB_PERSISTENCE', {
            offers        : 'SB_OFFERS',
            selectedOffer : 'SB_SELECTED_OFFER',
            user          : 'SB_USER',
            salesAgent    : 'SB_SALES_AGENT',
            firstSave     : 'SB_FIRST_SAVE',
            busyRetrieving: 'SB_BUSY_RETRIEVING'
        })
        .constant('SB_PORTALS_LOGIN', {
            url: (function() {
                var url = location.hostname.toLowerCase();
                if (url.indexOf('rapid.shop.t-mobile') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('respip.shop.t-mobile') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('staging.shop.t-mobile') !== -1) {
                    return 'https://www-1a3.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('waterfall.shop.t-mobile') !== -1) {
                    return 'https://www-1a1.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else if (url.indexOf('tmobile-') !== -1) {
                    return 'https://www-1a2.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
                else {
                    return 'https://www.t-mobile.nl/winkelwagen-opslaan?shopUrl=';
                }
            })()
        })
        //.constant('SB_DATALAYER', {
        //    package : {
        //        PackageActivationCost: undefined,
        //        PackageMID: undefined,
        //        PackageQuantity: '1',
        //        PackageContractDuration: undefined,
        //        PackageContractType: undefined,
        //        PackageMRC: undefined,
        //        PackageMRCDiscount: undefined,
        //        PackageOneTimeCost: undefined,
        //        PackageValue: undefined,
        //        Products: []
        //    },
        //    product : {
        //        ProductActivationCost: undefined,
        //        ProductBrand: undefined,
        //        ProductColor: undefined,
        //        ProductDescription: undefined,
        //        ProductListPrice: undefined,
        //        ProductMID: undefined,
        //        ProductMRC: undefined,
        //        ProductMRCDuration: undefined,
        //        ProductMRCDiscount: undefined,
        //        ProductMRCDiscountDuration: undefined,
        //        ProductMRCDiscountDescription: undefined,
        //        ProductName: undefined,
        //        ProductOneTimeCost: undefined,
        //        ProductType: undefined,
        //        ProductCode: undefined
        //    }
        //});
})();
